# Lista przykładowych komend u-boot w SCB POTATO_PI_M02

# Spis treści

1. [Ogólne](#ogólne)
2. [Test DDR3L](#test-ddr3l)
3. [Test LED](#test-led)
4. [Globalna konfiguracja GPIO](#globalna-konfiguracja-gpio)
5. [Test GPIO](#test-gpio)
    1. [Piny pomocnicze](#piny-pomocnicze)
    2. [Piny w złączu Expansion P1](#piny-w-złączu-expansion-p1)
    3. [Piny w złączu LCD-TFT P2](#piny-w-złączu-lcd-tft-p2)
6. [Testy karty SD](#testy-karty-sd)
7. [Test Ethernet](#test-ethernet)
8. [Test USB](#test-usb)
9. [Uruchomienie kernel Linux z karty SD](#uruchomienie-kernel-linux-z-karty-sd)
10. [Uruchomienie kernel Linux z serwera TFTP](#uruchomienie-kernel-linux-z-serwera-tftp)

## Ogólne

```
> version

U-Boot 2022.10-stm32mp-r1-dirty (Aug 26 2023 - 12:00:09 +0200)

arm-ostl-linux-gnueabi-gcc (GCC) 12.2.0
GNU ld (GNU Binutils) 2.40.20230119
```

## Test DDR3L

```
> mtest 0xC0000000 0xC1000000 0x12345678 1

Testing c0000000 ... c1000000:
Pattern 12345678  Writing...  Reading...Tested 1 iteration(s) with 0 errors.
```

## Test LED

```
led list
led led-red toggle
```

## Globalna konfiguracja GPIO

```
pinmux status -a
```

## Test GPIO

Rozkaz `GPIO` umożliwia sprawdzenie połączeń elektrycznych na PCB.

### Piny pomocnicze

```
gpio toggle GPIOA13         # LED_STATUS
gpio toggle GPIOI7          # PWR_VBUS_ON
gpio input GPIOA10          # VBUS_STATUS
gpio input GPIOG12          # ETH1_GPIO_RESET#
gpio input GPIOG6           # SDMMC1.DET#
gpio input GPIOI4           # BOOT0
gpio input GPIOI5           # BOOT1
gpio input GPIOI6           # BOOT2
gpio toggle GPIOF14         # SWCLK
gpio toggle GPIOF15         # SWDIO
```

### Piny w złączu Expansion P1

```
gpio toggle GPIOE13         # I2C5_SDA
gpio toggle GPIOH13         # I2C5_SCL
gpio toggle GPIOE6          # MCO2
gpio toggle GPIOA12         # USART1_RTS
gpio toggle GPIOG10         # SPI5_SCK
gpio toggle GPIOH12         # SPI5_MOSI
gpio toggle GPIOC0          # SPI1_MOSI
gpio toggle GPIOC3          # SPI1_MISO
gpio toggle GPIOB1          # SPI1_SCK
gpio toggle GPIOD3          # I2C1_SDA
gpio toggle GPIOE0          # UART8_RX
gpio toggle GPIOG0          # FDCAN2_TX
gpio toggle GPIOB5          # FDCAN2_RX
gpio toggle GPIOF11         # SAI1_FS_A
gpio toggle GPIOB9          # FDCAN1_TX

gpio toggle GPIOA0          # SAI1_SD_B
gpio toggle GPIOA5          # SAI1_SD_A
gpio toggle GPIOE3          # FDCAN1_RX
gpio toggle GPIOA11         # USART1_CTS
gpio toggle GPIOB8          # I2C1_SCL
gpio toggle GPIOE12         # UART8_DE
gpio toggle GPIOF12         # SPI1_NSS
gpio toggle GPIOE4          # UART8_TX
gpio toggle GPIOH11         # SPI5_NSS
gpio toggle GPIOG8          # SPI5_MISO
gpio toggle GPIOA4          # SAI1_SCK_A
gpio toggle GPIOD14         # USART1_RX
gpio toggle GPIOA9          # USART1_TX
```

### Piny w złączu LCD-TFT P2

```
gpio toggle GPIOE2          # LTDC.SCL (I2C4_SCL)
gpio toggle GPIOB7          # LTDC.SDA (I2C4_SDA)
gpio toggle GPIOH9          # LTDC.DE
gpio toggle GPIOE9          # LTDC.HSYNC
gpio toggle GPIOB15         # LTDC.CLK
gpio toggle GPIOF1          # LTDC.B7
gpio toggle GPIOD15         # LTDC.B5
gpio toggle GPIOE7          # LTDC.B3
gpio toggle GPIOG1          # LTDC.G7
gpio toggle GPIOD10         # LTDC.G5
gpio toggle GPIOF3          # LTDC.G3
gpio toggle GPIOC6          # LTDC.R6
gpio toggle GPIOE1          # LTDC.R4

gpio toggle GPIOB12         # LTDC.R3
gpio toggle GPIOF5          # LTDC.R5
gpio toggle GPIOD11         # LTDC.R7
gpio toggle GPIOF7          # LTDC.G2
gpio toggle GPIOD5          # LTDC.G4
gpio toggle GPIOC7          # LTDC.G6
gpio toggle GPIOH14         # LTDC.B4
gpio toggle GPIOB4          # LTDC.B6
gpio toggle GPIOG4          # LTDC.VSYNC
```

## Testy karty SD

```
> mmc list

STM32 SD/MMC: 0 (SD)

> mmc dev 0

switch to partitions #0, OK
mmc0 is current device

> mmc info

Device: STM32 SD/MMC
Manufacturer ID: 27
OEM: 5048
Name: SD16G
Bus Speed: 50000000
Mode: SD High Speed (50MHz)
Rd Block Len: 512
SD version 3.0
High Capacity: Yes
Capacity: 14.5 GiB
Bus Width: 4-bit
Erase Group Size: 512 Bytes

> mmc part

Partition Map for MMC device 0  --   Partition Type: EFI

Part    Start LBA       End LBA         Name
        Attributes
        Type GUID
        Partition GUID
  1     0x00000022      0x00000221      "fsbl1"
        attrs:  0x0000000000000000
        type:   8da63339-0007-60c0-c436-083ac8230908
                (8da63339-0007-60c0-c436-083ac8230908)
        guid:   ec183568-bdc8-4521-9105-fbd9efa8f87f
  2     0x00000222      0x00000421      "fsbl2"
        attrs:  0x0000000000000000
        type:   8da63339-0007-60c0-c436-083ac8230908
                (8da63339-0007-60c0-c436-083ac8230908)
        guid:   d90a5d95-e0a8-4461-a2d7-38daea3e3dd1
  3     0x00000422      0x00002421      "fip"
        attrs:  0x0000000000000000
        type:   19d5df83-11b0-457b-be2c-7559c13142a5
                (19d5df83-11b0-457b-be2c-7559c13142a5)
        guid:   a693b469-4b5b-4339-b6c6-d6432d2b2601
  4     0x00002422      0x00002821      "u-boot-env"
        attrs:  0x0000000000000000
        type:   3de21764-95bd-54bd-a5c3-4abe786f38a8
                (u-boot-env)
        guid:   49d4c7d0-8cf4-43de-9e51-d2b1185e5490
  5     0x00002822      0x00022821      "bootfs"
        attrs:  0x0000000000000004
        type:   0fc63daf-8483-4772-8e79-3d69d8477de4
                (linux)
        guid:   bed9e8e9-05b5-4573-98bd-095cfa2a168b
  6     0x00022822      0x00222822      "rootfs"
        attrs:  0x0000000000000000
        type:   0fc63daf-8483-4772-8e79-3d69d8477de4
                (linux)
        guid:   fa43c0c0-7684-4f97-bfd4-c46de2ac658d

> ext4ls mmc 0:5 /

<DIR>       1024 .
<DIR>       1024 ..
<DIR>      12288 lost+found
           51304 stm32mp135f-dk.dtb
         8043072 uImage

> ext4ls mmc 0:6 /

<DIR>       4096 .
<DIR>       4096 ..
<DIR>      16384 lost+found
<DIR>       4096 bin
<DIR>       4096 boot
<DIR>       4096 dev
<DIR>       4096 etc
<DIR>       4096 home
<DIR>       4096 lib
<DIR>       4096 media
<DIR>       4096 mnt
<DIR>       4096 proc
<DIR>       4096 run
<DIR>       4096 sbin
<DIR>       4096 srv
<DIR>       4096 sys
<DIR>       4096 tmp
<DIR>       4096 usr
<DIR>       4096 var
<DIR>       4096 vendor
<DIR>       4096 root
```

## Test Ethernet

```
> mii info

PHY 0x00: OUI = 0x01F0, Model = 0x13, Rev = 0x01, 100baseT, FDX

> net list

eth0 : eth1@5800a000 00:00:00:00:00:01 active

> dhcp

eth1@5800a000 Waiting for PHY auto negotiation to complete... done
BOOTP broadcast 1
*** Unhandled DHCP Option in OFFER/ACK: 213
DHCP client bound to address 192.168.0.200 (34 ms)
```

## Test USB

Wymagania:
- Włożyć sformatowany (może być FAT32) pendrive do gniazda USB nr 1 (wyższego).

```
> usb start

starting USB...
Bus usb@5800d000: USB EHCI 1.00
scanning bus usb@5800d000 for devices... 2 USB Device(s) found
       scanning usb for storage devices... 1 Storage Device(s) found
       
> usb tree

USB device tree:
  1  Hub (480 Mb/s, 0mA)
  |  u-boot EHCI Host Controller
  |
  +-2  Mass Storage (480 Mb/s, 200mA)
                USB DISK 2.0     07741700003E
                
> usb info

1: Hub,  USB Revision 2.0
 - u-boot EHCI Host Controller
 - Class: Hub
 - PacketSize: 64  Configurations: 1
 - Vendor: 0x0000  Product 0x0000 Version 1.0
   Configuration: 1
   - Interfaces: 1 Self Powered 0mA
     Interface: 0
     - Alternate Setting 0, Endpoints: 1
     - Class Hub
     - Endpoint 1 In Interrupt MaxPacket 8 Interval 255ms

2: Mass Storage,  USB Revision 2.0
 -          USB DISK 2.0     07741700003E
 - Class: (from Interface) Mass Storage
 - PacketSize: 64  Configurations: 1
 - Vendor: 0x13fe  Product 0x1d00 Version 1.16
   Configuration: 1
   - Interfaces: 1 Bus Powered 200mA
     Interface: 0
     - Alternate Setting 0, Endpoints: 2
     - Class Mass Storage, Transp. SCSI, Bulk only
     - Endpoint 1 In Bulk MaxPacket 512
     - Endpoint 2 Out Bulk MaxPacket 512
     
> usb storage

  Device 0: Vendor:          Rev: PMAP Prod: USB DISK 2.0
            Type: Removable Hard Disk
            Capacity: 1968.0 MB = 1.9 GB (4030464 x 512)
```

## Uruchomienie kernel Linux z karty SD

Wymagania:
- Karta SD stworzona zgodnie z [procedurą](https://gitlab.com/wojrus-projects/embedded/potato_pi_m02_sw/-/blob/main/basic_boot_chain/basic_boot_chain_guide_PL.md).

```
ext4load mmc 0:5 $kernel_addr_r /uImage
ext4load mmc 0:5 $fdt_addr_r /stm32mp135f-dk.dtb
setenv bootargs root=/dev/mmcblk0p6 rootwait rw console=ttySTM0,115200
bootm $kernel_addr_r - $fdt_addr_r
```

## Uruchomienie kernel Linux z serwera TFTP

Wymagania:
- Uruchomiony serwer TFTP na hoście np. [TFTPD64](https://bitbucket.org/phjounin/tftpd64/downloads/) (wersja dla Windows 10).
- W katalogu roboczym serwera TFTP powinne znajdować się pliki z partycji `bootfs` z karty SD.

```
dhcp
setenv serverip SERVER_IP_ADDRESS
tftpboot $kernel_addr_r uImage
tftpboot $fdt_addr_r stm32mp135f-dk.dtb
setenv bootargs root=/dev/mmcblk0p6 rootwait rw console=ttySTM0,115200
bootm $kernel_addr_r - $fdt_addr_r
```

# Licencja

MIT
