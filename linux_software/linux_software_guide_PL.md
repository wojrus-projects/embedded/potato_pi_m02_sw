# Różne aplikacje w systemie Linux w SCB POTATO_PI_M02

# Spis treści

1. [gammu](#gammu)
2. [Microsoft .NET SDK](#microsoft-net-sdk)

## gammu

Dokumentacja: https://docs.gammu.org/

[gammu](https://wammu.eu/gammu/) jest programem CLI do zarządzania m.in. SMS w telefonach komórkowych i modemach przemysłowych.

Wymagania:
- Linux zbudowany według [procedury](https://gitlab.com/wojrus-projects/embedded/potato_pi_m02_sw/-/tree/main/basic_boot_chain).
- Zainstalowany sterownik do FT232R według [procedury](../linux_hardware/linux_hardware_guide_PL.md#ft232cp210x-usb-serial-converter).
- Podłączony przykładowy [modem USB](https://gitlab.com/wojrus-projects/altium-designer/modem_m95_v1_1) z modułem Quectel M95.

Instalacja:

```
> apt update
> apt install gammu
```

Utworzyć plik konfiguracyjny `.gammurc` zawierający:

```
[gammu]
device = /dev/ttyUSB0
connection = at
```

Włączyć modem i sprawdzić komunikację z nim:

```
> gammu identify

Device               : /dev/ttyUSB0
Manufacturer         : Quectel_Ltd
Model                : unknown (Quectel_M95)
Firmware             : M95FAR02A08
IMEI                 : 862261048xxxxxx
SIM IMSI             : 260060030xxxxxx
```

Wysyłanie kodów USSD (przykład: sprawdzenie stanu konta w `Play`):

```
> gammu getussd '*101#'

Press Ctrl+C to break
USSD received
Status               : Terminated
Service reply        : "Pozostalo Ci 5,00zl. Mozesz korzystac ze srodkow bezterminowo."
```

Odczytanie wszystkich SMS:

```
> gammu getallsms
```

Wysłanie SMS:

```
# Variant #1:
> echo "Test SMS 1" | gammu sendsms TEXT PHONE_NUMBER

# Variant #2:
> gammu sendsms TEXT PHONE_NUMBER -text "Test SMS 2"
```

Wykonanie rozmowy:

```
> gammu dialvoice PHONE_NUMBER
```

## Microsoft .NET SDK

Dokumentacja:
- https://dotnet.microsoft.com/en-us/download/dotnet
- https://learn.microsoft.com/en-us/dotnet/core/tools

Wymagania:
- Linux zbudowany według [procedury](https://gitlab.com/wojrus-projects/embedded/potato_pi_m02_sw/-/tree/main/basic_boot_chain).

Instalacja:

```
> apt update
> apt upgrade
> apt install icu

> mkdir -p $HOME/dotnet
> cd $HOME/dotnet
> wget https://download.visualstudio.microsoft.com/download/pr/6a909983-cf0f-4b23-823d-8db56fdb344f/6455cb1f9a9a0eebc8fa541d08d7717c/dotnet-sdk-8.0.302-linux-arm.tar.gz
> tar zxf dotnet-sdk-8.0.302-linux-arm.tar.gz -C $HOME/dotnet
```

Do pliku `$HOME/.bash_profile` (w przykładzie był używany BASH) dodać poniższe linie i zresetować SBC:

```
export DOTNET_ROOT=$HOME/dotnet
export PATH=$PATH:$HOME/dotnet
export DOTNET_CLI_TELEMETRY_OPTOUT=1
```

Upewnić się czy zmienne środowiskowe są poprawnie ustawione:

```
> env
```

Test instalacji:

```
> dotnet -h
> dotnet --list-runtimes
> dotnet --list-sdks
```

Test kompilacji projektów C#:

```
> mkdir -p $HOME/dotnet/project
> cd $HOME/dotnet/project
> dotnet new list
> dotnet new console -o HelloWorld
> dotnet new classlib -o HelloWorldCore
> dotnet new sln -n Projects
> dotnet sln add HelloWorld/HelloWorld.csproj
> dotnet sln add HelloWorldCore/HelloWorldCore.csproj
> dotnet build

# Run application:
> HelloWorld/bin/Debug/net8.0/HelloWorld

> dotnet clean
```

# Licencja

MIT
