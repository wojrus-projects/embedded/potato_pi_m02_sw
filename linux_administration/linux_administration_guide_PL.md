# Zarządzanie systemem Linux w SCB POTATO_PI_M02

# Spis treści

1. [Logowanie](#logowanie-do-sbc)
2. [Komendy podstawowe](#komendy-podstawowe)
    1. [Wersja systemu](#wersja-systemu)
    2. [Zajętość RAM](#zajętość-ram)
    3. [Zajętość systemu plików](#zajętość-systemu-plików)
    4. [Interfejsy sieciowe](#interfejsy-sieciowe)
    5. [Montowanie dysku USB](#montowanie-dysku-usb)
3. [Pakiety APT](#pakiety-apt)
4. [Taktowanie CPU](#taktowanie-cpu)
5. [Temperatura CPU](#temperatura-cpu)
6. [Lista procesów](#lista-procesów)
7. [systemd: zarządzanie](#systemd-zarządzanie)
8. [systemd: RTC](#systemd-rtc)
9. [Test pamięci DDR](#test-pamięci-ddr)
10. [BASH](#bash)

## Logowanie do SBC

SBC musi być uruchomione z karty SD z zainstalowaną dystrybucją Linux według [procedury](https://gitlab.com/wojrus-projects/embedded/potato_pi_m02_sw/-/tree/main/basic_boot_chain).

Do SBC można zalogować się na 3 sposoby:
1. SSH (port 22) - metoda zalecana do celów administracyjnych.
2. UART (gniazdo P3, 115200 bps 8E1)
3. Lokalnie z użyciem monitora VGA i klawiatury USB.

Parametry logowania: user=`root`, password=`root`.

## Komendy podstawowe

### Wersja systemu

```
> uname -a

Linux stm32mp1 6.1.28 #1 SMP PREEMPT Sat Aug 26 12:22:25 CEST 2023 armv7l GNU/Linux

> cat /proc/version

Linux version 6.1.28 (wr@debian) (arm-ostl-linux-gnueabi-gcc (GCC) 12.2.0, GNU ld (GNU Binutils) 2.40.20230119) #1 SMP PREEMPT Sat Aug 26 12:22:25 CEST 2023

> cat /etc/build

-----------------------
Build Configuration:  |
-----------------------
BB_VERSION = 2.4.0
BUILD_SYS = x86_64-linux
NATIVELSBSTRING = universal
TARGET_SYS = arm-ostl-linux-gnueabi
MACHINE = stm32mp1
DISTRO = openstlinux-weston
DISTRO_VERSION = 4.2.1-openstlinux-6.1-yocto-mickledore-mp1-v23.06.21
TUNE_FEATURES = arm vfp cortexa7 neon vfpv4 thumb callconvention-hard
TARGET_FPU = hard
MANIFESTVERSION = ostl-23-04-26-rc7-2-gebc8636
DISTRO_CODENAME = mickledore
ACCEPT_EULA_stm32mp1 = 1
GCCVERSION = 12.%
PREFERRED_PROVIDER_virtual/kernel = linux-stm32mp
[...]
```

### Zajętość RAM

```
> free -hw

               total        used        free      shared     buffers       cache   available
Mem:           441Mi        79Mi       212Mi       9.1Mi        15Mi       153Mi       361Mi
Swap:             0B          0B          0B
Total:         441Mi        79Mi       212Mi
```

### Zajętość systemu plików

```
> df -h

Filesystem      Size  Used Avail Use% Mounted on
/dev/root       948M  554M  334M  63% /
devtmpfs        189M     0  189M   0% /dev
tmpfs           221M     0  221M   0% /dev/shm
tmpfs            89M  9.0M   80M  11% /run
tmpfs           4.0M     0  4.0M   0% /sys/fs/cgroup
tmpfs           221M     0  221M   0% /tmp
/dev/mmcblk0p5   55M  7.8M   43M  16% /boot
tmpfs           221M   88K  221M   1% /var/volatile
tmpfs            45M     0   45M   0% /run/user/0
```

### Interfejsy sieciowe

```
> ifconfig -a

can0      Link encap:UNSPEC  HWaddr 00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00
          NOARP  MTU:16  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:10
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)
          Interrupt:55

can1      Link encap:UNSPEC  HWaddr 00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00
          NOARP  MTU:16  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:10
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)
          Interrupt:56

end0      Link encap:Ethernet  HWaddr 00:00:00:00:00:01
          inet addr:192.168.0.200  Bcast:192.168.0.255  Mask:255.255.255.0
          inet6 addr: fdc4:2b44:1780:500:200:ff:fe00:1/64 Scope:Global
          inet6 addr: fe80::200:ff:fe00:1/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:160 errors:0 dropped:123 overruns:0 frame:0
          TX packets:96 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:11284 (11.0 KiB)  TX bytes:14861 (14.5 KiB)
          Interrupt:57 Base address:0x8000

lo        Link encap:Local Loopback
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:218 errors:0 dropped:0 overruns:0 frame:0
          TX packets:218 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:14717 (14.3 KiB)  TX bytes:14717 (14.3 KiB)
```

### Montowanie dysku USB

Uwaga: dysk może pobrać maksymalnie 1.0 A prądu z gniazda USB w SBC. Prąd jest ograniczony sprzętowo przez dedykowane zabezpieczenie w SBC.

Podłączyć dysk HDD 2.5" (przez odpowiedni interfejs) lub Flash do gniazda USB i sprawdzić czy został wykryty przez OS (w przykładzie poniżej jest dysk `sda` + partycja `sda1`):

```
> lsblk -f

NAME        FSTYPE FSVER LABEL  UUID                                 FSAVAIL FSUSE% MOUNTPOINTS
sda
`-sda1      exfat        Laptop A865-D927
mtdblock0
mmcblk0
|-mmcblk0p1
|-mmcblk0p2
|-mmcblk0p3
|-mmcblk0p4
|-mmcblk0p5 ext4         bootfs ec9969f5-f44f-4bf1-95b8-8ab67c354f35   43.1M    13% /boot
`-mmcblk0p6 ext4         rootfs d4cf1d6b-46f9-4cc1-b98d-3799d01c8843    1.2G    54% /
```

Zamontować partycję z dysku:

```
> mkdir /media/usb
> mount /dev/sda1 /media/usb
```

Odmontowanie partycji:

```
> umount /media/usb
```

Gdy dysk był sformatowany pod Windows jako `exfat` to należy wcześniej zainstalować obsługę tego standardu:

```
> apt install fuse-exfat
```

## Pakiety APT

Aktualizacja listy pakietów i samych pakietów z repozytorium firmy ST:

```
> apt update
> apt upgrade
```

Lista wszystkich dostępnych pakietów (3 metody):

```
> apt-cache search KEYWORD | more
> apt-cache search . | grep PATTERN | more
> apt list | grep PATTERN | more
```

Różne przydatne pakiety do zainstalowania:

```
> apt install -y mc nano
> apt install -y links
> apt install -y fbida
> apt install -y stress dhrystone htop
> apt install -y binutils ldd git cmake python3-pip
```

Lista pakietów zainstalowanych:

```
> apt list --installed | more
```

## Taktowanie CPU

Szczegółowe informacje o aktualnym taktowaniu CPU:

```
> cpufreq-info
```

Dokumentacja: https://docs.kernel.org/admin-guide/pm/cpufreq.html

```
> cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies
> cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_cur_freq
> cat /sys/devices/system/cpu/cpu0/cpufreq/stats/trans_table
> cat /sys/devices/system/cpu/cpu0/cpufreq/stats/time_in_state
> cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
```

Ustawienie maksymalnego stałego taktowania:

```
> echo "performance" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
```

## Temperatura CPU

```
> cat /sys/class/thermal/thermal_zone0/temp
```

Wynik należy podzielić przez 1000.

## Lista procesów

Wersja minimalistyczna:

```
> ps -AlF
```

Prosta lista:

```
> top
```

Zaawansowana lista z obsługą myszą (wymaga `apt install htop`):

```
> htop
```

## systemd: zarządzanie 

Bibliografia:<br>
- https://www.dobreprogramy.pl/@mariushko/poradnik-systemd-cz.-1,blog,67627
- https://www.dobreprogramy.pl/@mariushko/poradnik-systemd-cz.-2,blog,67878
- https://www.dobreprogramy.pl/@mariushko/poradnik-systemd-cz.-3,blog,68067

Lista niepoprawnie działających *units*:

```
> systemctl --failed

  UNIT                             LOAD   ACTIVE SUB    DESCRIPTION
* weston-graphical-session.service loaded failed failed Weston graphical session
```

```
> systemctl --all --failed
> systemctl --all --type=error
> systemctl --all --type=not-found
> systemctl --all --failed --type=error --type=not-found --no-legend

* home.mount                       not-found inactive dead   home.mount
* auditd.service                   not-found inactive dead   auditd.service
* connman.service                  not-found inactive dead   connman.service
* dropbear.service                 not-found inactive dead   dropbear.service
* plymouth-quit-wait.service       not-found inactive dead   plymouth-quit-wait.service
* plymouth-start.service           not-found inactive dead   plymouth-start.service
* run-postinsts.service            not-found inactive dead   run-postinsts.service
* weston-graphical-session.service loaded    failed   failed Weston graphical session
* cryptsetup.target                not-found inactive dead   cryptsetup.target
```

Lista usług:

```
> systemctl -t service
```

Włączenie/wyłączenie usługi:

```
> systemctl enable NAME
> systemctl disable NAME
```

Zatrzymanie/uruchomienie/status usługi:

```
> systemctl stop NAME
> systemctl start NAME
> systemctl restart NAME
> systemctl status NAME
```

Przeglądanie logu (ostatnie 20 linii):

```
> journalctl -n 20
```

Pokaż błędy w logu:

```
> journalctl -p err
```

Monitorowanie logu w trybie ciągłym:

```
> journalctl -f
```

Postać szczegółowa:

```
> journalctl -n 1 -o verbose
```

Ilość zajmowanego miejsca przez log:

```
> journalctl --disk-usage
```

Bezpieczne skasowanie loga:

```
> journalctl -m --vacuum-time=1s
```

Inwazyjne skasowanie całego loga:

```
> rm -rf /run/log/journal/*
```

## systemd: RTC

Sprawdzenie czasu:

```
> timedatectl

               Local time: Wed 2023-10-04 06:46:31 UTC
           Universal time: Wed 2023-10-04 06:46:31 UTC
                 RTC time: n/a
                Time zone: Universal (UTC, +0000)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no
```

Ustawienie strefy czasowej:

```
> timedatectl list-timezones
> timedatectl set-timezone Europe/Warsaw
```

Wyłącz/włącz synchronizację czasu:

```
> timedatectl set-ntp off
> timedatectl set-ntp on
```

Ręczne ustawienie czasu:

```
> timedatectl set-time '2023-10-04 10:00:00'
```

## Test pamięci DDR

Test wykonany narzędziem `memtester`:

```
> memtester 100M 1

memtester version 4.6.0 (32-bit)
Copyright (C) 2001-2020 Charles Cazabon.
Licensed under the GNU General Public License version 2 (only).

pagesize is 4096
pagesizemask is 0xfffff000
want 100MB (104857600 bytes)
got  100MB (104857600 bytes), trying mlock ...locked.
Loop 1/1:
  Stuck Address       : ok
  Random Value        : ok
  Compare XOR         : ok
  Compare SUB         : ok
  Compare MUL         : ok
  Compare DIV         : ok
  Compare OR          : ok
  Compare AND         : ok
  Sequential Increment: ok
  Solid Bits          : ok
  Block Sequential    : ok
  Checkerboard        : ok
  Bit Spread          : ok
  Bit Flip            : ok
  Walking Ones        : ok
  Walking Zeroes      : ok

Done.
```

## BASH

Przydatne linki:
- https://www.shellcheck.net/

Standardowa [dystrybucja](https://wiki.st.com/stm32mpu/wiki/OpenSTLinux_distribution) Linux na SBC ma zainstalowaną i uruchamianą powłokę `sh`, która jest niekompatybilna z bardziej popularnym `bash` ([porównanie](https://stackoverflow.com/questions/5725296/difference-between-sh-and-bash)).

Sprawdzenie obecnej wersji `sh`:

```
> env | grep SHELL

SHELL=/bin/sh

> sh --help

BusyBox v1.36.0 () multi-call binary.

> file -h /bin/sh

/bin/sh: symbolic link to /bin/busybox.nosuid
```

Ponieważ `bash` jest już zainstalowany to można używać nowego formatu skryptów `.sh` przez dodanie `#!/bin/bash` w pierwszej linii.

```
> /bin/bash --version

GNU bash, version 5.2.15(1)-release (arm-ostl-linux-gnueabi)
```

Trwała zmiana powłoki wymaga edycji ustawień użytkownika `root` (zmienić `sh` na `bash`):

```
> nano /etc/passwd
```

Dodać plik pomocny do automatycznego uruchamiania procesów po zalogowaniu:

```
> touch $HOME/.bash_profile
```

# Licencja

MIT
