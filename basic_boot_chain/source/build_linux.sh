#!/bin/bash

# Original procedure:
# $HOME/stm32mp1/stm32mp1-openstlinux-6.1-yocto-mickledore-mp1-v23.06.21/sources/arm-ostl-linux-gnueabi/linux-stm32mp-6.1.28-stm32mp-r1-r0/README.HOW_TO.txt

set -euo pipefail

echo "Check ARCH: $ARCH"
echo "Check CROSS_COMPILE: $CROSS_COMPILE"
echo "Check CC:"
$CC --version
echo "Check SDK version: $OECORE_SDK_VERSION"

BUILD_THREADS_NUMBER=3

DISTRIBUTION=stm32mp1-openstlinux-6.1-yocto-mickledore-mp1-v23.06.21

cd $DISTRIBUTION/sources/arm-ostl-linux-gnueabi/linux-stm32mp-6.1.28-stm32mp-r1-r0/linux-6.1.28

#
# Copy DTS overlays
#

mkdir -p arch/arm/boot/dts/overlays
cp -v $STM32MP1_OVERLAYS_DIR/*.dts arch/arm/boot/dts/overlays

#
# Build kernel and modules
#

# Use to add/remove modules and set options
# make ARCH=arm menuconfig

make ARCH=arm uImage vmlinux dtbs LOADADDR=0xC2000040 -j$BUILD_THREADS_NUMBER
make ARCH=arm modules -j$BUILD_THREADS_NUMBER

#
# Install binaries
#

KERNEL_INSTALL_DIR=$STM32MP1_INSTALL_DIR/kernel

! rm -r $KERNEL_INSTALL_DIR

mkdir -p $KERNEL_INSTALL_DIR
mkdir -p $KERNEL_INSTALL_DIR/boot
mkdir -p $KERNEL_INSTALL_DIR/boot/overlays

make ARCH=arm INSTALL_MOD_PATH="$KERNEL_INSTALL_DIR" modules_install

cp -v arch/arm/boot/uImage $KERNEL_INSTALL_DIR/boot
cp -v arch/arm/boot/dts/stm32mp135f-dk.dtb $KERNEL_INSTALL_DIR/boot
cp -v arch/arm/boot/dts/overlays/*.dtbo $KERNEL_INSTALL_DIR/boot/overlays

cd $KERNEL_INSTALL_DIR
! rm lib/modules/*/source
! rm lib/modules/*/build

find . -name "*.ko" | xargs ${CROSS_COMPILE}strip --strip-debug --remove-section=.comment --remove-section=.note --preserve-dates

echo "Linux kernel and modules binaries path: $KERNEL_INSTALL_DIR"
echo "Done"
