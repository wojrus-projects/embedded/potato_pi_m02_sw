#!/bin/bash

set -euo pipefail

if [ "$#" -ne 1 ]
then
    echo "Missing device name"
    exit 1
fi

SD_CARD=$1

echo "Select SD card '$SD_CARD'"

if [ ! -b $SD_CARD ]
then
    echo "Device error"
    exit 1
fi

INSTALL_DIR=install

echo "Zero-fill all sectors on partitions"
sudo dd if=/dev/zero of=${SD_CARD}1 bs=256K count=1    status=progress oflag=sync
sudo dd if=/dev/zero of=${SD_CARD}2 bs=256K count=1    status=progress oflag=sync
sudo dd if=/dev/zero of=${SD_CARD}3 bs=1M   count=4    status=progress oflag=sync
sudo dd if=/dev/zero of=${SD_CARD}4 bs=512K count=1    status=progress oflag=sync
sudo dd if=/dev/zero of=${SD_CARD}5 bs=1M   count=64   status=progress oflag=sync
sudo dd if=/dev/zero of=${SD_CARD}6 bs=1M   count=3072 status=progress oflag=sync

echo "Write partition 1: atf_potato-pi-m02_sd-card.stm32"
sudo dd if=$INSTALL_DIR/atf/atf_potato-pi-m02_sd-card.stm32 of=${SD_CARD}1 bs=1M status=progress oflag=sync

echo "Write partition 2: atf_potato-pi-m02_sd-card.stm32"
sudo dd if=$INSTALL_DIR/atf/atf_potato-pi-m02_sd-card.stm32 of=${SD_CARD}2 bs=1M status=progress oflag=sync

echo "Write partition 3: fip.bin"
sudo dd if=$INSTALL_DIR/fip.bin of=${SD_CARD}3 bs=1M status=progress oflag=sync

# References:
# - "u-boot,mmc-env-partition" in u-boot/doc/device-tree-bindings/config.txt
# - CONFIG_ENV_SIZE option in u-boot .config file
ENV_PARTITION_SIZE_KB=512
CONFIG_ENV_SIZE_KB=8
let ENV_OFFSET_KB=ENV_PARTITION_SIZE_KB-CONFIG_ENV_SIZE_KB

echo "Write partition 4: u-boot-env.bin at offset $ENV_OFFSET_KB KB"
sudo dd if=$INSTALL_DIR/uboot/u-boot-env.bin of=${SD_CARD}4 bs=1k status=progress oflag=sync skip=0 seek=$ENV_OFFSET_KB

echo "Write partition 5: bootfs.ext4"
sudo dd if=bootfs.ext4 of=${SD_CARD}5 bs=1M status=progress oflag=sync

echo "Write partition 6: rootfs.ext4"
sudo dd if=rootfs_st.ext4 of=${SD_CARD}6 bs=1M status=progress oflag=sync

echo "Done"
