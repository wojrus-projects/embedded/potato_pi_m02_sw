#!/bin/bash

# Script run syntax:
# source prepare_env.sh

source $HOME/stm32mp1/sdk/environment-setup-cortexa7t2hf-neon-vfpv4-ostl-linux-gnueabi

export STM32MP1_INSTALL_DIR=$HOME/stm32mp1/install
export STM32MP1_OVERLAYS_DIR=$HOME/stm32mp1/overlays

echo "Done"
