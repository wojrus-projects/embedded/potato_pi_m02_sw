#!/bin/bash

set -euo pipefail

git clone https://github.com/STMicroelectronics/optee_os.git --branch 3.19.0-stm32mp-r1 --single-branch
cd optee_os
git checkout -b WORKING 3.19.0-stm32mp-r1

patch -p1 < ../optee_os_potato_pi_m02.patch
cd ..

echo "Done"
