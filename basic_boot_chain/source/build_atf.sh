#!/bin/bash

set -euo pipefail

echo "Check ARCH: $ARCH"
echo "Check CROSS_COMPILE: $CROSS_COMPILE"
echo "Check CC:"
$CC --version
echo "Check SDK version: $OECORE_SDK_VERSION"

cd arm-trusted-firmware

# Available log levels:
# 0  (LOG_LEVEL_NONE)
# 10 (LOG_LEVEL_ERROR)
# 20 (LOG_LEVEL_NOTICE)
# 30 (LOG_LEVEL_WARNING)
# 40 (LOG_LEVEL_INFO)
# 50 (LOG_LEVEL_VERBOSE)

BUILD_LOG_LEVEL=40

INSTALL_DIR=$STM32MP1_INSTALL_DIR/atf

! rm -r $INSTALL_DIR
mkdir -p $INSTALL_DIR

unset LDFLAGS;
unset CFLAGS;

echo "Build with STM32MP_USB_PROGRAMMER=1"
make realclean
make ARM_ARCH_MAJOR=7 ARCH=aarch32 PLAT=stm32mp1 STM32MP_USB_PROGRAMMER=1 STM32MP13=1 AARCH32_SP=optee DTB_FILE_NAME=stm32mp135f-dk.dtb LOG_LEVEL=$BUILD_LOG_LEVEL STM32MP_EARLY_CONSOLE=1
cp -v build/stm32mp1/release/tf-a-stm32mp135f-dk.stm32 $INSTALL_DIR/atf_potato-pi-m02_usb.stm32

echo "Build with STM32MP_SDMMC=1"
make realclean
make ARM_ARCH_MAJOR=7 ARCH=aarch32 PLAT=stm32mp1 STM32MP_SDMMC=1 STM32MP13=1 AARCH32_SP=optee DTB_FILE_NAME=stm32mp135f-dk.dtb LOG_LEVEL=$BUILD_LOG_LEVEL STM32MP_EARLY_CONSOLE=1
cp -v build/stm32mp1/release/tf-a-stm32mp135f-dk.stm32 $INSTALL_DIR/atf_potato-pi-m02_sd-card.stm32

cp -v build/stm32mp1/release/fdts/stm32mp135f-dk-fw-config.dtb $INSTALL_DIR/stm32mp135f-dk-fw-config.dtb

echo "Binary files are in $INSTALL_DIR"
echo "Done"
