#!/bin/bash

set -euo pipefail

echo "Check ARCH: $ARCH"
echo "Check CROSS_COMPILE: $CROSS_COMPILE"
echo "Check CC:"
$CC --version
echo "Check SDK version: $OECORE_SDK_VERSION"

cd u-boot

INSTALL_DIR=$STM32MP1_INSTALL_DIR/uboot

! rm -r $INSTALL_DIR
mkdir -p $INSTALL_DIR

unset LDFLAGS;
unset CFLAGS;

make DEVICE_TREE=stm32mp135f-dk all

# Check CONFIG_ENV_SIZE
mkenvimage -s 0x2000 -p 0x00 -o u-boot-env.bin ../u-boot-env.txt

cp -v u-boot-nodtb.bin $INSTALL_DIR
cp -v u-boot.dtb $INSTALL_DIR
mv -v u-boot-env.bin $INSTALL_DIR

echo "Binary files are in $INSTALL_DIR"
echo "Done"
