#!/bin/bash

set -euo pipefail

git clone https://github.com/STMicroelectronics/arm-trusted-firmware.git --branch v2.8-stm32mp-r1 --single-branch
cd arm-trusted-firmware
git checkout -b WORKING v2.8-stm32mp-r1

patch -p1 < ../atf_potato_pi_m02.patch
cd ..

echo "Done"
