#!/bin/bash

set -euo pipefail

echo "Check ARCH: $ARCH"
echo "Check CROSS_COMPILE: $CROSS_COMPILE"
echo "Check CC:"
$CC --version
echo "Check SDK version: $OECORE_SDK_VERSION"

cd optee_os

INSTALL_DIR=$STM32MP1_INSTALL_DIR/optee

! rm -r $INSTALL_DIR
mkdir -p $INSTALL_DIR

# Available log levels: 0-4, default=2
BUILD_LOG_LEVEL=2

! rm -r build

unset LDFLAGS;
unset CFLAGS;

make clean
make PLATFORM=stm32mp1 CFG_EMBED_DTB_SOURCE_FILE=stm32mp135f-dk.dts CFG_TEE_CORE_LOG_LEVEL=$BUILD_LOG_LEVEL O=build all

cp -v build/core/tee-header_v2.bin $INSTALL_DIR
cp -v build/core/tee-pageable_v2.bin $INSTALL_DIR
cp -v build/core/tee-pager_v2.bin $INSTALL_DIR

echo "Binary files are in $INSTALL_DIR"
echo "Done"
