#!/bin/bash

# https://wiki.st.com/stm32mpu/wiki/STM32MP1_Developer_Package

set -euo pipefail

DISTRIBUTION=stm32mp1-openstlinux-6.1-yocto-mickledore-mp1-v23.06.21

! rm -rf $DISTRIBUTION/sdk
! rm -rf $DISTRIBUTION/sources
! rm -rf $DISTRIBUTION/images

echo "Install SDK"

SDK=en.SDK-x86_64-$DISTRIBUTION

if [ -f ${SDK}.tar.gz ]
then
    gzip -d ${SDK}.tar.gz
fi

echo "Unpack ${SDK}.tar"
tar xf ${SDK}.tar

echo "Install SDK"
./stm32mp1-openstlinux-6.1-yocto-mickledore-mp1-v23.06.21/sdk/st-image-weston-openstlinux-weston-stm32mp1-x86_64-toolchain-4.2.1-openstlinux-6.1-yocto-mickledore-mp1-v23.06.21.sh -d $HOME/stm32mp1/sdk

echo "Install sources"

SOURCES=en.sources-$DISTRIBUTION

if [ -f ${SOURCES}.tar.gz ]
then
    gzip -d ${SOURCES}.tar.gz
fi

echo "Unpack ${SOURCES}.tar"
tar xf ${SOURCES}.tar

echo "Install images"

IMAGES=en.flash-stm32mp1-openstlinux-6-1-yocto-mickledore-mp1-v23-06-21

if [ -f ${IMAGES}.tar.gz ]
then
    gzip -d ${IMAGES}.tar.gz
fi

echo "Unpack ${IMAGES}.tar"
tar xf ${IMAGES}.tar

echo "Copy rootfs"
cp -v stm32mp1-openstlinux-6.1-yocto-mickledore-mp1-v23.06.21/images/stm32mp1/st-image-weston-openstlinux-weston-stm32mp1.ext4 rootfs_st.ext4

echo "Copy vendorfs"
cp -v stm32mp1-openstlinux-6.1-yocto-mickledore-mp1-v23.06.21/images/stm32mp1/st-image-vendorfs-openstlinux-weston-stm32mp1.ext4 vendorfs_st.ext4

echo "Done"
