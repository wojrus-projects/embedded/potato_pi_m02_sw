#!/bin/bash

# Original procedure:
# $HOME/stm32mp1/stm32mp1-openstlinux-6.1-yocto-mickledore-mp1-v23.06.21/sources/arm-ostl-linux-gnueabi/linux-stm32mp-6.1.28-stm32mp-r1-r0/README.HOW_TO.txt

set -euo pipefail

echo "Check ARCH: $ARCH"
echo "Check CROSS_COMPILE: $CROSS_COMPILE"
echo "Check CC:"
$CC --version
echo "Check SDK version: $OECORE_SDK_VERSION"

DISTRIBUTION=stm32mp1-openstlinux-6.1-yocto-mickledore-mp1-v23.06.21

cd $DISTRIBUTION/sources/arm-ostl-linux-gnueabi/linux-stm32mp-6.1.28-stm32mp-r1-r0

! rm -rf linux-6.1.28

echo "Unpack linux-6.1.28.tar.xz"
tar xf linux-6.1.28.tar.xz

cd linux-6.1.28

echo "Create local Git repo from TAR"
test -d .git || git init . && git add . && git commit -m "Custom kernel"
git checkout -b WORKING

# Disable auto-generation of kernel version number.
echo "" > .scmversion

echo "Apply patches"
for p in `ls -1 ../*.patch`; do git am $p; done

make ARCH=arm multi_v7_defconfig "fragment*.config"
for f in `ls -1 ../fragment*.config`; do scripts/kconfig/merge_config.sh -m -r .config $f; done
! yes '' | make ARCH=arm oldconfig

echo "Apply SBC POTATO_PI_M02 patch"
patch -p1 < $HOME/stm32mp1/linux_6.1.28_potato_pi_m02.patch

echo "Set custom .config"
cp -v $HOME/stm32mp1/linux_6.1.28_potato_pi_m02.config .config

echo "Done"
