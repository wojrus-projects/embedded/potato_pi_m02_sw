#!/bin/bash

set -euo pipefail

git clone https://github.com/STMicroelectronics/u-boot.git --branch v2022.10-stm32mp-r1 --single-branch
cd u-boot
git checkout -b WORKING v2022.10-stm32mp-r1

make stm32mp13_defconfig

patch -p1 < ../u-boot_potato_pi_m02.patch
cp -v ../u-boot_potato_pi_m02.config .config
cd ..

echo "Done"
