#!/bin/bash

set -euo pipefail

INSTALL_DIR=$STM32MP1_INSTALL_DIR

OPTEE_PATH=$INSTALL_DIR/optee
ATF_PATH=$INSTALL_DIR/atf
UBOOT_PATH=$INSTALL_DIR/uboot

! rm $INSTALL_DIR/fip.bin

fiptool update \
--tos-fw $OPTEE_PATH/tee-header_v2.bin \
--tos-fw-extra1 $OPTEE_PATH/tee-pager_v2.bin \
--tos-fw-extra2 $OPTEE_PATH/tee-pageable_v2.bin \
--fw-config $ATF_PATH/stm32mp135f-dk-fw-config.dtb \
--hw-config $UBOOT_PATH/u-boot.dtb \
--nt-fw $UBOOT_PATH/u-boot-nodtb.bin \
$INSTALL_DIR/fip.bin

fiptool info $INSTALL_DIR/fip.bin

echo "fip.bin is in $INSTALL_DIR"
echo "Done"
