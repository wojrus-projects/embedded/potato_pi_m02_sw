#!/bin/bash

set -euo pipefail

if [ "$#" -ne 1 ]
then
    echo "Missing device name"
    exit 1
fi

SD_CARD=$1

echo "Select SD card '$SD_CARD'"

if [ ! -b $SD_CARD ]
then
    echo "Device error"
    exit 1
fi

echo "Delete all partitions"
sudo sgdisk -Z $SD_CARD

echo "Add new GPT partitions"
sudo sgdisk --resize-table=128 -a 1         \
        -n 1:34:545         -c 1:fsbl1      \
        -n 2:546:1057       -c 2:fsbl2      \
        -n 3:1058:9249      -c 3:fip        \
        -n 4:9250:10273     -c 4:u-boot-env \
        -n 5:10274:141345   -c 5:bootfs     \
        -n 6:141346:6432801 -c 6:rootfs     \
        $SD_CARD

echo "Set partition type"
sudo sgdisk -t 1:8DA63339-0007-60C0-C436-083AC8230908 \
            -t 2:8DA63339-0007-60C0-C436-083AC8230908 \
            -t 3:19D5DF83-11B0-457b-BE2C-7559C13142A5 \
            -t 4:3DE21764-95BD-54BD-A5C3-4ABE786F38A8 \
            -t 5:0FC63DAF-8483-4772-8E79-3D69D8477DE4 \
            -t 6:0FC63DAF-8483-4772-8E79-3D69D8477DE4 \
            $SD_CARD

echo "Select bootable partition"
sudo sgdisk -A 5:set:2 $SD_CARD

echo "Print partitions info"
sudo sgdisk -p $SD_CARD

echo "Done"
