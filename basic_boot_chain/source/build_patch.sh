#!/bin/bash

set -euo pipefail

PATCH_DIR=$PWD

TIMESTAMP=$(date +%Y%m%d_%H%M%S)

PATCH_DIR_BACKUP=$PATCH_DIR/patch_backup/$TIMESTAMP

mkdir -p $PATCH_DIR_BACKUP

echo "Backup patches to $PATCH_DIR_BACKUP"
cp -v $PATCH_DIR/*.patch $PATCH_DIR_BACKUP


cd arm-trusted-firmware

echo "atf"
git diff > $PATCH_DIR/atf_potato_pi_m02.patch


cd ..
cd optee_os

echo "optee"
git diff > $PATCH_DIR/optee_os_potato_pi_m02.patch


cd ..
cd u-boot

echo "u-boot"
git diff > $PATCH_DIR/u-boot_potato_pi_m02.patch


cd ..

DISTRIBUTION=stm32mp1-openstlinux-6.1-yocto-mickledore-mp1-v23.06.21

cd $DISTRIBUTION/sources/arm-ostl-linux-gnueabi/linux-stm32mp-6.1.28-stm32mp-r1-r0/linux-6.1.28

echo "linux"
git diff > $PATCH_DIR/linux_6.1.28_potato_pi_m02.patch

echo "Done"
