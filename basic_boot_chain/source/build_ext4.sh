#!/bin/bash

set -euo pipefail

KERNEL_INSTALL_DIR=$HOME/stm32mp1/install/kernel

! sudo umount /media/bootfs
! sudo umount /media/rootfs
! sudo umount /media/vendorfs

echo "Create file bootfs.ext4"

truncate --size=64M bootfs.ext4
dd if=/dev/zero of=bootfs.ext4 bs=1M count=64 status=progress

echo "Format bootfs.ext4"

sudo mkfs.ext4 -L bootfs -cv bootfs.ext4

echo "Copy files to bootfs.ext4"

sudo mkdir -p /media/bootfs
sudo mount -t ext4 -o loop bootfs.ext4 /media/bootfs

sudo rm -r /media/bootfs/*
sudo cp -rv $KERNEL_INSTALL_DIR/boot/* /media/bootfs

sudo umount /media/bootfs

echo "Update rootfs_st.ext4"

sudo mkdir -p /media/rootfs
sudo mkdir -p /media/vendorfs
sudo mount -t ext4 -o loop rootfs_st.ext4 /media/rootfs
sudo mount -t ext4 -o loop vendorfs_st.ext4 /media/vendorfs

echo "Update /lib/modules in rootfs_st.ext4"

sudo rm -r /media/rootfs/lib/modules/*
sudo cp -r $KERNEL_INSTALL_DIR/lib/modules/* /media/rootfs/lib/modules

echo "Update /vendor/lib in rootfs_st.ext4"

sudo mkdir -p /media/rootfs/vendor/lib
sudo cp -rv /media/vendorfs/lib/* /media/rootfs/vendor/lib

sudo umount /media/rootfs
sudo umount /media/vendorfs

echo "Done"
