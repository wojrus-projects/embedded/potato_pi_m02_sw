#!/bin/bash

set -euo pipefail

sudo apt update
sudo apt install -y git
sudo apt install -y arm-trusted-firmware-tools
sudo apt install -y device-tree-compiler
sudo apt install -y u-boot-tools
sudo apt install -y python3-full python3-pip python3-pyelftools python3-pil
sudo apt install -y libncurses5-dev libncursesw5-dev
sudo apt install -y libssl-dev libyaml-dev bc

echo "Done"
