# Kompilacja "boot chain" i Linux na SBC POTATO_PI_M02

# Spis treści

1. [Cel projektu](#1-cel-projektu)
2. [Założenia](#2-założenia)
3. [Wymagania](#3-wymagania)
4. [Przygotowanie hosta](#4-przygotowanie-hosta)
5. [Przygotowanie SDK](#5-przygotowanie-sdk)
6. [ARM Trusted Firmware (FSBL)](#6-arm-trusted-firmware-fsbl)
7. [Test FSBL](#7-test-fsbl)
8. [OP-TEE](#8-op-tee)
9. [u-boot (SSBL)](#9-u-boot-ssbl)
10. [Plik FIP](#10-plik-fip)
11. [Test SSBL](#11-test-ssbl)
12. [Kernel Linux](#12-kernel-linux)
13. [Tworzenie obrazów partycji EXT4 (bootfs+rootfs)](#13-tworzenie-obrazów-partycji-ext4-bootfsrootfs)
14. [Tworzenie partycji na karcie SD](#14-tworzenie-partycji-na-karcie-sd)
15. [Zapisanie plików/obrazów na kartę SD](#15-zapisanie-plikówobrazów-na-kartę-sd)
16. [Uruchomienie Linux](#16-uruchomienie-linux)

## 1. Cel projektu

Celem projektu jest zbudowanie kompletnego i funkcjonalnego systemu Linux, który będzie wykorzystywany do dalszych prac R&D z użyciem [POTATO_PI_M02](https://gitlab.com/wojrus-projects/altium-designer/potato_pi_m02_v1_0) (np. testy modułów HAT).

Dodatkowo będzie utworzone środowisko programistyczne do budowania w przyszłości dowolnych programów i modułów kernel na SBC.

Dzięki dostępności u-boot można wykonać niskopoziomową diagnostykę SBC lub ładować i uruchamiać inne oprogramowanie niż Linux.

## 2. Założenia

- Oprogramowanie do POTATO_PI_M02 wykorzystuje w dużym stopniu BSP/HAL/DTS od płytki uruchomieniowej [STM32MP135F-DK](https://www.st.com/en/evaluation-tools/stm32mp135f-dk.html).
- Użyte będzie wyłącznie oryginalne oprogramowanie dostarczone przez firmę ST Microelectronics (źródła, toolchain i rootfs) z nałożonymi łatkami specyficznymi dla POTATO_PI.
- Skrypty są podzielone na:
   - konfiguracyjne (`prepare_xxx.sh`) - powinny być uruchomione tylko jeden raz.
   - budujące (`build_xxx.sh`) - można je modyfikować i uruchamiać wiele razy.
- Kody źródłowe będą używane w wersji GIT w celu łatwego tworzenia łatek (*patch*).
- Dla lepszego zrozumienia poniższej procedury konieczna jest lektura https://wiki.st.com/stm32mpu

## 3. Wymagania

1. Etap budowania kodu dla SBC i tworzenia obrazów partycji na karcie SD:

   - Debian 12.1.0 (host)
   - VirtualBox 7.0.10 (opcja gdy host ma działać pod Windows 10)
   - Czytnik kart SD na USB podłączony do hosta i opcjonalnie zamontowany w VirtualBox
   - Karta micro SD 4GB+
   - Minimum 20GB wolnego miejsca na partycji roboczej hosta

2. Etap testowania i rozruchu SBC przez USB OTG (wersja dla Windows):

   - Windows 10+
   - [STM32CubeProgrammer](https://www.st.com/en/development-tools/stm32cubeprog.html) 2.14
   - Kabel USB host-to-host (2x wtyczka typu A)
   - Kabel/interfejs UART z sygnałami TXD+RXD+GND, poziomy napięciowe +3.3V, z przewodami sygnałowymi pasującymi do pinów (headers) raster 2.54mm
   - Terminal tekstowy np. [TeraTerm](https://ttssh2.osdn.jp/index.html.en)

## 4. Przygotowanie hosta

Założenia:
- Wszystkie prace będą prowadzone w katalogu `$HOME/stm32mp1`.
- Finalne wyniki kompilacji będą umieszczone w katalogu `$HOME/stm32mp1/install`.

```
mkdir -p $HOME/stm32mp1
mkdir -p $HOME/stm32mp1/install
cd $HOME/stm32mp1
```

Skopiować wszystkie pliki z projektu **basic_boot_chain/source** do katalogu `$HOME/stm32mp1`.

Zaktualizować oprogramowanie hosta:

```
./prepare_host.sh
```

Proces budowania oprogramowania wykorzystuje GIT do tworzenia łatek.<br>Gdy GIT nie był wcześniej używany to należy ustawić domyślną konfigurację użytkownika lokalnych repozytoriów:

```
# Check:
git config --global user.name
git config --global user.email

# Set:
git config --global user.name "your name"
git config --global user.email "your email"
```

## 5. Przygotowanie SDK

Pobrać ze strony [STM32MP1Dev (SDK+sources)](https://www.st.com/en/embedded-software/stm32mp1dev.html#get-software) oraz [STM32MP1Starter (flash)](https://www.st.com/en/embedded-software/stm32mp1starter.html#get-software) poniższe pliki i umieścić w katalogu `$HOME/stm32mp1`:

```
en.SDK-x86_64-stm32mp1-openstlinux-6.1-yocto-mickledore-mp1-v23.06.21.tar.gz
en.sources-stm32mp1-openstlinux-6.1-yocto-mickledore-mp1-v23.06.21.tar.gz
en.flash-stm32mp1-openstlinux-6-1-yocto-mickledore-mp1-v23-06-21.tar.gz
```

Do ściągnięcia plików może być wymagane założenie konta na stronie https://www.st.com.

> UWAGA: Do budowy oprogramowania na SBC muszą być używane wyłącznie powyższe pliki.<br>
> Każde odstępstwo np. użycie innej wersji GCC może skutkować problemami np. z modułami kernel Linux.

Wykonać w katalogu `$HOME/stm32mp1`:

```
./prepare_sdk.sh
```

Od teraz w każdej nowej konsoli używanej do budowania oprogramowania na SBC trzeba wykonać w katalogu `$HOME/stm32mp1`:

```
source prepare_env.sh
```

## 6. ARM Trusted Firmware (FSBL)

**ARM Trusted Firmware** trzeba zbudować w dwóch wersjach:
1. Do uruchomienia przez USB: parametr `STM32MP_USB_PROGRAMMER=1`
2. Do uruchomienia z karty SD: parametr `STM32MP_SDMMC=1`

Wykonać w katalogu `$HOME/stm32mp1`:

```
./prepare_atf.sh
./build_atf.sh
```

Pliki binarne będą umieszczone w `$HOME/stm32mp1/install/atf`

## 7. Test FSBL

Na tym etapie można wykonać test uruchomienia SBC i FSBL przez USB OTG.

- Muszą być usunięte zworki BOOT JP1/2/3.
- Podłączyć kabel UART do złącza P3.
- Na hoście uruchomić terminal z ustawieniami portu szeregowego:
   - 115200 baud
   - 8 data bits
   - 1 stop bit
   - even parity
- Podłączyć kabel USB host-to-host do portu 2 (bliżej PCB) w gnieździe USB J1.
- Wykonać (wersja dla Windows 10):

```
STM32_Programmer_CLI.exe -c port=usb1 -d atf_potato-pi-m02_usb.stm32 0x01 --start 0x01
```

## 8. OP-TEE

Wykonać w katalogu `$HOME/stm32mp1`:

```
./prepare_optee.sh
./build_optee.sh
```

Pliki binarne będą umieszczone w `$HOME/stm32mp1/install/optee`

## 9. u-boot (SSBL)

Wykonać w katalogu `$HOME/stm32mp1`:

```
./prepare_uboot.sh
./build_uboot.sh
```

Pliki binarne będą umieszczone w `$HOME/stm32mp1/install/uboot`

## 10. Plik FIP

Wykonać w katalogu `$HOME/stm32mp1`:

```
./build_fip.sh
```

Powinien powstać plik `$HOME/stm32mp1/install/fip.bin`

## 11. Test SSBL

Na tym etapie można wykonać test uruchomienia SBC i SSBL przez USB OTG.

Konfiguracja SBC jest identyczna jak w [Test FSBL](#Test-FSBL).

Wersja dla Windows 10:

```
STM32_Programmer_CLI.exe -c port=usb1 -d atf_potato-pi-m02_usb.stm32 0x01 -s 0x1 -d fip.bin 0x03 -s 0x03
```

## 12. Kernel Linux

Wykonać w katalogu `$HOME/stm32mp1`:

```
./prepare_linux.sh
./build_linux.sh
```

Pliki binarne będą umieszczone w `$HOME/stm32mp1/install/kernel`

## 13. Tworzenie obrazów partycji EXT4 (bootfs+rootfs)

Wykonać w katalogu `$HOME/stm32mp1`:

```
./build_ext4.sh
```

## 14. Tworzenie partycji na karcie SD

Za pomocą narzędzia
   `lsblk`
lub
   `sudo fdisk -l`
znaleźć nazwę urządzenia będącego kartą SD.

Po ustaleniu nazwy wykonać w katalogu `$HOME/stm32mp1`:

```
./sd_card_format.sh /dev/NAME
```

Zostanie utworzonych 6 partycji:

| # | Name       | Size    |
|---|------------|---------|
| 1 | fsbl1      | 256 KiB |
| 2 | fsbl2      | 256 KiB |
| 3 | fip        | 4 MiB   |
| 4 | u-boot-env | 512 KiB |
| 5 | bootfs     | 64 MiB  |
| 6 | rootfs     | 3 GiB   |

## 15. Zapisanie plików/obrazów na kartę SD

Wykonać w katalogu `$HOME/stm32mp1`:

```
./sd_card_write.sh /dev/NAME
```

## 16. Uruchomienie Linux

- Wyłączyć zasilanie SBC.
- Odłączyć kabel USB używany do testów FSBL/SSBL.
- Włożyć kartę SD do SBC.
- Podłączyć peryferia:
   - Klawiatura USB
   - Monitor VGA
   - Ethernet
   - Kabel UART jak w testach FSBL/SSBL.
- Ustawić zworki BOOT: JP1=INSERT, JP2=REMOVE, JP3=INSERT.
- Włączyć zasilanie SBC.

Powinien uruchomić się kernel Linux i następnie całe środowisko z partycji rootfs.
<br>LED *STATUS* (czerwona) powinna cyklicznie migać (*heart-beat*).
<br>Na monitorze VGA powinno pokazać się logo dystrybucji [OpenSTLinux](https://www.st.com/en/embedded-software/stm32-mpu-openstlinux-distribution.html) i następnie konsola tekstowa.
<br>Po kilku sekundach powinno uruchomić się środowisko graficzne [Weston](https://gitlab.freedesktop.org/wayland/weston/-/blob/master/README.md), które można obsługiwać myszą i klawiaturą USB.
<br>W `Wayland Terminal` można uruchomić np. program testowy `gtk3-demo`.

# Licencja

MIT
