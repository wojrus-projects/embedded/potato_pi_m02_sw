# "POTATO_PI_M02" software

Repozytorium zawiera oprogramowanie do SBC [POTATO_PI_M02](https://gitlab.com/wojrus-projects/altium-designer/potato_pi_m02_v1_0).

- [basic_boot_chain](basic_boot_chain/basic_boot_chain_guide_PL.md) - budowa i instalacja podstawowej dystrybucji Linux uruchamianej z karty micro SD.
- [u-boot](u-boot/u-boot_guide_PL.md) - przykłady rozkazów [Das U-Boot](https://wiki.st.com/stm32mpu/wiki/U-Boot_overview).
- [linux_administration](linux_administration/linux_administration_guide_PL.md) - zarządzanie systemem Linux na SBC.
- [linux_hardware](linux_hardware/linux_hardware_guide_PL.md) - instalacja i konfiguracja peryferiów w systemie Linux na SBC.
- [linux_software](linux_software/linux_software_guide_PL.md) - różne aplikacje w systemie Linux na SBC.

# Autor

Woj. Rus. <rwxrwx@interia.pl>

# Licencja

MIT
