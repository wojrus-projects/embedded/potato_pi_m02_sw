# Instalacja i konfiguracja peryferiów w systemie Linux w SCB POTATO_PI_M02

# Spis treści

1. [Device Tree overlays](#device-tree-overlays)
2. [Test NOR flash](#test-nor-flash)
    1. [Konfiguracja NOR flash](#konfiguracja-nor-flash)
    2. [Test integralności NOR flash](#test-integralności-nor-flash)
3. [Test interfejsu I2C](#test-interfejsu-i2c)
4. [Test interfejsu SPI](#test-interfejsu-spi)
5. [Test interfejsu CAN-FD](#test-interfejsu-can-fd)
6. [Test GPIO](#test-gpio)
7. [FT232/CP210x USB Serial Converter](#ft232cp210x-usb-serial-converter)
8. [CP2112/FT260 USB-to-I2C Bridge](#cp2112ft260-usb-to-i2c-bridge)
9. [LED triggers](#led-triggers)
10. [USB Smart Card reader (Identiv uTrust 2700 R)](#usb-smart-card-reader-identiv-utrust-2700-r)
11. [Bluetooth 4.0 USB dongle (LogiLink BT0037)](#bluetooth-40-usb-dongle-logilink-bt0037)
12. [Omnikey 6321 CLi (iCLASS NFC card reader)](#omnikey-6321-cli-iclass-nfc-card-reader)
13. [Impulsator obrotowy inkrementalny](#impulsator-obrotowy-inkrementalny)
14. [Czujnik temperatury TMP75C](#czujnik-temperatury-tmp75c)
15. [ADC Microchip MCP3426](#adc-microchip-mcp3426)
16. [Protokół One-Wire (OWFS)](#protokół-one-wire-owfs)
17. [Wyjście VGA](#wyjście-vga)
18. [LCD SPI ILI9341](#lcd-spi-ili9341)
19. [Stereo Audio DAC USB PCM2707](#stereo-audio-dac-usb-pcm2707)

## Device Tree overlays

Dodatkowe urządzenia i moduły sprzętowe, które nie występują w głównym pliku `Device Tree` (arch/arm/boot/dts/stm32mp135f-dk.dts), <br>wymagają stworzenia i aktywacji specjalnych łatek na DT zwanych `overlays`.

Przykładowe pliki overlays w postaci źródłowej DTS znajdują się w projekcie **potato_pi_m02_sw** w katalogu **basic_boot_chain/source/overlays**.

Po zbudowaniu Linux za pomocą [procedury](https://gitlab.com/wojrus-projects/embedded/potato_pi_m02_sw/-/blob/main/basic_boot_chain/basic_boot_chain_guide_PL.md) w katalogu roboczym `$HOME/stm32mp1/install/kernel/boot/overlays` powstaną pliki DTBO.
<br>Po zapisaniu karty SD za pomocą `sd_card_write.sh` pliki DTBO będą dostępne na partycji `bootfs` w katalogu `/boot/overlays`.

Standardowo wszystkie pliki overlays są nieaktywne. Ich aktywacją zajmuje się `u-boot`.
<br>W celu aktywacji konkretnych DT overlays wykonać:
- Uruchomić SBC z karty SD i zatrzymać start Linux (nacisnąć dowolny klawisz w ciągu 3s od uruchomienia u-boot).
- Dodać pliki DTBO do listy `fdtoverlay_files`. Nazwy plików powinny być bez rozszerzenia i rozdzielone spacją:

```
> env print fdtoverlay_files
> env set fdtoverlay_files "PREVIOUS-DTBO-FILES NEW-DTBO-FILES"
> env save
> reset
```

## Test NOR flash

### Konfiguracja NOR flash

```
> mtdinfo

Count of MTD devices:           1
Present MTD devices:            mtd0
Sysfs interface supported:      yes

> mtdinfo /dev/mtd0

mtd0
Name:                           spi0.0
Type:                           nor
Eraseblock size:                4096 bytes, 4.0 KiB
Amount of eraseblocks:          1024 (4194304 bytes, 4.0 MiB)
Minimum input/output unit size: 1 byte
Sub-page size:                  1 byte
Character device major/minor:   90:0
Bad blocks are allowed:         false
Device is writable:             true
```

### Test integralności NOR flash

```
# Prepare write data:
> dd if=/dev/urandom of=random.bin bs=4M count=1

1+0 records in
1+0 records out
4194304 bytes (4.2 MB, 4.0 MiB) copied, 0.202314 s, 20.7 MB/s

# Erase flash:
> mtd_debug erase /dev/mtd0 0 0x400000

Erased 4194304 bytes from address 0x00000000 in flash

# Write flash:
> mtd_debug write /dev/mtd0 0 0x400000 random.bin

Copied 4194304 bytes from random.bin to address 0x00000000 in flash

# Read flash:
> mtd_debug read /dev/mtd0 0 0x400000 read.bin

Copied 4194304 bytes from address 0x00000000 in flash to read.bin

# Compare files:
> cmp random.bin read.bin

# Erase again:
> mtd_debug erase /dev/mtd0 0 0x400000

Erased 4194304 bytes from address 0x00000000 in flash

# Read again:
> mtd_debug read /dev/mtd0 0 0x400000 read.bin

Copied 4194304 bytes from address 0x00000000 in flash to read.bin

# Verify erase:
> hexdump read.bin

0000000 ffff ffff ffff ffff ffff ffff ffff ffff
*
0400000
```

## Test interfejsu I2C

Dokumentacja: https://wiki.st.com/stm32mpu/wiki/I2C_i2c-tools

Linux w SBC ma standardowo aktywne 2 interfejsy I2C (#1 i #5).

Sygnały I2C (MPU pin / P1 pin):

| Name | I2C #1   | I2C #5   |
|------|----------|----------|
| SCL  | PB8 / 28 | PH13 / 5 |
| SDA  | PD3 / 27 | PE13 / 3 |

Podłączyć czujnik temperatury [TMP75C TI](https://www.ti.com/product/TMP75C) lub odpowiednik do pinów SCL, SDA i zasilania (+3.3 V = pin 1, GND = pin 6) w złączu P1.

```
# Show I2C buses.
> i2cdetect -l

i2c-0   i2c             STM32F7 I2C(0x40012000)                 I2C adapter
i2c-1   i2c             STM32F7 I2C(0x4c006000)                 I2C adapter

# Detect TMP75C on I2C #5 (7-bit address is 0x48).
> i2cdetect -y 1

     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:                         -- -- -- -- -- -- -- --
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
40: -- -- -- -- -- -- -- -- 48 -- -- -- -- -- -- --
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
70: -- -- -- -- -- -- -- --

# Read 4 16-bit registers from TMP75C.
> i2cdump -r 0x00-0x03 -y 1 0x48 w

     0,8  1,9  2,a  3,b  4,c  5,d  6,e  7,f
00: 701f 0000 004b 0050

# Read regiter 0 (temperature).
> i2cget -y 1 0x48 0x00 w

0xe01c      # T=+28.875 C

# Write 16-bit value to register 3 (T_high).
> i2cset -y 1 0x48 0x03 0xABCD w
> i2cget -y 1 0x48 0x03 w

0xa0cd      # Register 3 has writable 12 bits MSB.
```

## Test interfejsu SPI

### Device Tree overlay

Test SPI wymaga `DT overlay`, którego kod znajduje się w pliku `$HOME/stm32mp1/overlays/spidev-spiX.dts` (X=1 lub 5) i jest automatycznie kompilowany po wykonaniu `./build_linux.sh`.
<br>Overlay musi być włączony w `u-boot` zgodnie z [procedurą](#device-tree-overlays).

Przykład:

```
> env set fdtoverlay_files spidev-spi1 spidev-spi5
> env save
> reset
```

SBC udostępnia 2 interfejsy SPI (#1 i #5) w złączu P1 `Expansion`.

Sygnały SPI (MPU pin / P1 pin):

| Name  | SPI #1    | SPI #5    |
|-------|-----------|-----------|
| SCK   | PB1 / 23  | PG10 / 13 |
| MOSI  | PC0 / 19  | PH12 / 15 |
| MISO  | PC3 / 21  | PG8 / 16  |
| NSS   | PF12 / 24 | PH11 / 18 |

Zainstalować pakiet [spi-tools](https://github.com/cpb-/spi-tools):

```
> apt update
> apt install spitools
```

Zewrzeć sygnały MOSI i MISO w wybranym SPI w złączu P1 i wykonać testy:

```
# Get available SPI devices:
> ls /dev | grep spidev

spidev1.0
spidev2.0

# Get interface configuration:
> spi-config -q -d /dev/spidev1.0

/dev/spidev1.0: mode=0, lsb=0, bits=8, speed=10000000, spiready=0

# Send bytes sequence 0x01-0x77-0xF3 and see reply:
> printf '\x01\x77\xF3' | spi-pipe -d /dev/spidev1.0 | hexdump -C

00000000  01 77 f3                                          |.w.|
00000003
```

## Test interfejsu CAN-FD

Dokumentacja: https://github.com/linux-can/can-utils/blob/master/README.md

Linux w SBC ma standardowo aktywne 2 interfejsy CAN-FD (#1 i #2).

Sygnały SPI (MPU pin / P1 pin):

| Name   | CAN #1   | CAN #2   |
|--------|----------|----------|
| CAN_TX | PB9 / 37 | PG0 / 31 |
| CAN_RX | PE3 / 36 | PB5 / 33 |

Sprawdzenie uruchomienia sterowników CAN:

```
> dmesg | grep m_can

[    2.507033] m_can_platform 4400e000.can: sidf 0x0 0 xidf 0x0 0 rxf0 0x0 32 rxf1 0x900 0 rxb 0x900 0 txe 0x900 2 txb 0x910 2
[    2.508039] m_can_platform 4400e000.can: m_can device registered (irq=55, version=32)
[    2.524924] m_can_platform 4400f000.can: sidf 0x1400 0 xidf 0x1400 0 rxf0 0x1400 32 rxf1 0x1d00 0 rxb 0x1d00 0 txe 0x1d00 2 txb 0x1d10 2
[    2.525944] m_can_platform 4400f000.can: m_can device registered (irq=56, version=32)
```

Konfiguracja interfejsów CAN:

```
> ip link set can0 type can bitrate 1000000 dbitrate 2000000 fd on
> ip link set can1 type can bitrate 1000000 dbitrate 2000000 fd on
```

Lista dostępnych opcji konfiguracyjnych CAN:

```
> ip link set can0 type can help
```

Uruchomienie interfejsów CAN:

```
> ip link set can0 up
> ip link set can1 up
```

Wysłanie danych:

```
> cansend can0 123#1122334455667788
> cansend can1 123#1122334455667788
```

Odbiór danych:

```
> candump can0
> candump can1
```

Status interfejsu:

```
> ip -details link show can0
> ip -details link show can1
```

Włączenie loopback:

```
> ip link set can0 down
> ip link set can1 down
> ip link set can0 up type can bitrate 1000000 dbitrate 2000000 fd on loopback on
> ip link set can1 up type can bitrate 1000000 dbitrate 2000000 fd on loopback on
> ip link set can0 up
> ip link set can1 up
```

Wysłanie danych do siebie z użyciem loopback:

```
> candump can0 -L &
> cansend can0 300#AC.AB.AD.AE.75.49.AD.D1

(1695466757.305678) can0 300#ACABADAE7549ADD1
(1695466757.305643) can0 300#ACABADAE7549ADD1
```

## Test GPIO

Lista wszystkich portów GPIO:

```
> gpiodetect
```

Wyświetlenie informacji o wszystkich portach GPIO:

```
> cat /sys/kernel/debug/gpio

[...]

> gpioinfo

gpiochip0 - 16 lines:
        line   0:       "PA0"                   input
        line   1:       "PA1"                   input
        line   2:       "PA2"                   input
        line   3:       "PA3"                   input
        line   4:       "PA4"                   input
        line   5:       "PA5"                   input
        line   6:       "PA6"                   input
        line   7:       "PA7"                   input consumer="kernel"
        line   8:       "PA8"                   input
        line   9:       "PA9"                   input
        line  10:       "PA10"                  input
        line  11:       "PA11"                  input
        line  12:       "PA12"                  input
        line  13:       "PA13"                  output active-low consumer="red:heartbeat"
        line  14:       "PA14"                  input
        line  15:       "PA15"                  input
gpiochip1 - 16 lines:
        line   0:       "PB0"                   input
[...]
```

Wyświetlenie informacji o jednym porcie GPIO:

```
> gpioinfo -c gpiochip1
```

Odczytanie stanu wejścia:

```
> gpioget -c gpiochip4 12

"0"=inactive
```

Ustawienie wyjścia:

```
> gpioset -c gpiochip4 12=0
[Ctrl + C]
```

Cykliczna zmiana stanu wyjścia:

```
> gpioset -t 500ms -c gpiochip4 12=active 
[Ctrl + C]
```

## FT232/CP210x USB Serial Converter

Wymagania:
- Środowisko do budowy kernel Linux na SBC według [procedury](https://gitlab.com/wojrus-projects/embedded/potato_pi_m02_sw/-/tree/main/basic_boot_chain).

Włączenie sterowników FT232/CP210x w konfiguracji kernel Linux:

```
#
# Configuration stage
#

> cd $HOME/stm32mp1
> source prepare_env.sh
> cd stm32mp1-openstlinux-6.1-yocto-mickledore-mp1-v23.06.21/sources/arm-ostl-linux-gnueabi/linux-stm32mp-6.1.28-stm32mp-r1-r0/linux-6.1.28
> make ARCH=arm menuconfig

Device Drivers > USB support > USB Serial Converter support

[*]   USB Generic Serial Driver
 <M>   USB CP210x family of UART Bridge Controllers
 <M>   USB FTDI Single Port Serial Driver

#
# Build stage
#

> cd $HOME/stm32mp1
> ./build_linux.sh
> ./build_ext4.sh
> ./sd_card_write /dev/SD_CARD_DEVICE
```

Podłączyć interfejs z FT232 lub CP210x i wykonać reset SBC oraz testy:

```
> lsusb

Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 002 Device 002: ID 0403:6001 Future Technology Devices International, Ltd FT232 Serial (UART) IC
Bus 002 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hub

> dmesg | grep FTDI

[   16.111149] usbserial: USB Serial support registered for FTDI USB Serial Device
[   16.117280] ftdi_sio 2-1:1.0: FTDI USB Serial Device converter detected
[   16.151308] usb 2-1: FTDI USB Serial Device converter now attached to ttyUSB0

> ls /dev/ttyUSB*

/dev/ttyUSB0

> echo "Hello" > /dev/ttyUSB0
```

## CP2112/FT260 USB-to-I2C Bridge

Wymagania:
- Środowisko do budowy kernel Linux na SBC według [procedury](https://gitlab.com/wojrus-projects/embedded/potato_pi_m02_sw/-/tree/main/basic_boot_chain).

Włączenie sterowników CP2112/FT260 w konfiguracji kernel Linux:

```
#
# Configuration stage
#

> cd $HOME/stm32mp1
> source prepare_env.sh
> cd stm32mp1-openstlinux-6.1-yocto-mickledore-mp1-v23.06.21/sources/arm-ostl-linux-gnueabi/linux-stm32mp-6.1.28-stm32mp-r1-r0/linux-6.1.28
> make ARCH=arm menuconfig

Device Drivers > HID support

[*]   /dev/hidraw  raw HID devices support

Special HID drivers --->

<M>   Silicon Labs CP2112 HID USB-to-SMBus Bridge support
<M>   FTDI FT260 USB HID to I2C host support

#
# Build stage
#

> cd $HOME/stm32mp1
> ./build_linux.sh
> ./build_ext4.sh
> ./sd_card_write /dev/SD_CARD_DEVICE
```

Podłączyć interfejs z CP2112 lub FT260 i wykonać reset SBC oraz testy:

```
> lsusb

Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 002 Device 002: ID 10c4:ea90 Silicon Labs CP2112 HID I2C Bridge
Bus 002 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hub

> dmesg | grep CP2112

[    4.122649] usb 2-1: Product: CP2112 HID USB-to-SMBus Bridge
[   31.106514] cp2112 0003:10C4:EA90.0001: hidraw0: USB HID v1.01 Device [Silicon Laboratories CP2112 HID USB-to-SMBus Bridge] on usb-5800c000.usb-1/input0
[   31.197534] i2c_dev: adapter [CP2112 SMBus Bridge on hidraw0] registered as minor 2
[   31.198187] i2c i2c-2: adapter [CP2112 SMBus Bridge on hidraw0] registered

> i2cdetect -l

i2c-0   i2c             STM32F7 I2C(0x40012000)                 I2C adapter
i2c-1   i2c             STM32F7 I2C(0x4c006000)                 I2C adapter
i2c-2   i2c             CP2112 SMBus Bridge on hidraw0          I2C adapter
```

## LED triggers

Lista LED:

```
> ls -la /sys/class/leds

red:heartbeat -> ../../devices/platform/leds/leds/red:heartbeat
```

Wybranie LED STATUS (red):

```
> cd /sys/class/leds/red:heartbeat
```

Lista triggers:

```
> cat trigger

none usb-gadget usb-host kbd-scrolllock kbd-numlock kbd-capslock kbd-kanalock kbd-shiftlock kbd-altgrlock kbd-ctrllock kbd-altlock kbd-shiftllock kbd-shiftrlock kbd-ctrlllock kbd-ctrlrlock timer oneshot disk-activity disk-read disk-write ide-disk mtd nand-disk [heartbeat] backlight gpio cpu cpu0 activity default-on transient netdev pattern tty mmc0 stmmac-0:00:link stmmac-0:00:100Mbps stmmac-0:00:10Mbps
```

Zatrzymanie *heartbeat*:

```
> echo none > trigger
```

Ciągłe włączenie/wyłączenie LED:

```
> echo 1 > brightness
> echo 0 > brightness
```

Cykliczne włączanie LED za pomocą timera:

```
> echo timer > trigger
> echo 500 > delay_off
> echo 50 > delay_on
```

Aktywność karty SD:

```
> echo mmc0 > trigger
```

Aktywność interfejsu Ethernet `end0` (link+TX+RX):

```
> echo netdev > trigger
> echo end0 > device_name
> echo 50 > interval
> echo 1 > link
> echo 1 > tx
> echo 1 > rx
```

Aktywność interfejsu CAN-FD `can0` (TX+RX) (test wymaga włączonego loopback w ustawieniach CAN-FD):

```
> echo netdev > trigger
> echo can0 > device_name
> echo 50 > interval
> echo 1 > tx
> echo 1 > rx
```

## USB Smart Card reader (Identiv uTrust 2700 R)

Strona produktu: https://www.identiv.com/products/logical-access-control/smart-card-readers-writers/contact-smart-card-readers-writers/2700r

Support: https://support.identiv.com/2700r

Log `dmesg`:

```
[  118.590915] usb 2-1: new full-speed USB device number 2 using ohci-platform
[  118.871008] usb 2-1: New USB device found, idVendor=04e6, idProduct=5810, bcdDevice= 2.02
[  118.878128] usb 2-1: New USB device strings: Mfr=1, Product=2, SerialNumber=5
[  118.888119] usb 2-1: Product: uTrust 2700 R Smart Card Reader
[  118.894506] usb 2-1: Manufacturer: Identiv
[  118.897194] usb 2-1: SerialNumber: 536917432xxxxx
```

Instalacja narzędzi PCSC:

```
> apt install pcsc-lite
> apt install pcsc-tools
> apt install opensc
```

Test czytnika i kart:

```
> pcsc_scan

PC/SC device scanner
V 1.6.2 (c) 2001-2022, Ludovic Rousseau <ludovic.rousseau@free.fr>
Using reader plug'n play mechanism
Scanning present readers...
0: Identive CLOUD 2700 R Smart Card Reader [CCID Interface] (536917432xxxxx) 00 00

Sat Sep 30 10:22:18 2023
 Reader 0: Identive CLOUD 2700 R Smart Card Reader [CCID Interface] (536917432xxxxx) 00 00
  Event number: 0
  Card state: Card removed,

Sat Sep 30 10:22:22 2023
 Reader 0: Identive CLOUD 2700 R Smart Card Reader [CCID Interface] (536917432xxxxx) 00 00
  Event number: 1
  Card state: Card inserted,
  ATR: 3B 6E 00 00 80 31 80 66 B0 84 0C 01 6E 01 83 00 90 00

[...]
```

```
> opensc-tool -l

# Detected readers (pcsc)
Nr.  Card  Features  Name
0    Yes             Identive CLOUD 2700 R Smart Card Reader [CCID Interface] (536917432xxxxx) 00 00
```

## Bluetooth 4.0 USB dongle (LogiLink BT0037)

Strona produktu: https://www.2direct.de/notebook-computer/bluetooth/2520/bluetooth-4.0-adapter-usb-2.0-usb-a

Dokumentacja:<br>
- https://wiki.st.com/stm32mpu/wiki/Bluetooth_overview<br>
- https://wiki.st.com/stm32mpu/wiki/How_to_set_up_a_Bluetooth_connection<br>
- https://wiki.st.com/stm32mpu/wiki/How_to_scan_Bluetooth_devices

Log `dmesg`:

```
> dmesg | grep Bluetooth

[    0.093264] Bluetooth: Core ver 2.22
[    0.093375] Bluetooth: HCI device and connection manager initialized
[    0.093393] Bluetooth: HCI socket layer initialized
[    0.093405] Bluetooth: L2CAP socket layer initialized
[    0.093436] Bluetooth: SCO socket layer initialized
[    0.458563] Bluetooth: RFCOMM TTY layer initialized
[    0.458596] Bluetooth: RFCOMM socket layer initialized
[    0.458642] Bluetooth: RFCOMM ver 1.11
[    0.458679] Bluetooth: BNEP (Ethernet Emulation) ver 1.3
[    0.458687] Bluetooth: BNEP filters: protocol multicast
[    0.458702] Bluetooth: BNEP socket layer initialized
[    0.458709] Bluetooth: HIDP (Human Interface Emulation) ver 1.2
[    0.458721] Bluetooth: HIDP socket layer initialized
[    4.400527] Bluetooth: hci0: CSR: Setting up dongle with HCI ver=6 rev=22bb; LMP ver=6 subver=22bb; manufacturer=10
[   22.901933] Bluetooth: MGMT ver 1.22
```

Konfiguracja interfejsu:

```
> hciconfig -a

hci0:   Type: Primary  Bus: USB
        BD Address: 00:15:83:E8:3C:56  ACL MTU: 310:10  SCO MTU: 64:8
        DOWN
        RX bytes:582 acl:0 sco:0 events:30 errors:0
        TX bytes:367 acl:0 sco:0 commands:30 errors:0
        Features: 0xff 0xff 0x8f 0xfe 0xdb 0xff 0x5b 0x87
        Packet type: DM1 DM3 DM5 DH1 DH3 DH5 HV1 HV2 HV3
        Link policy: RSWITCH HOLD SNIFF PARK
        Link mode: PERIPHERAL ACCEPT

> hciconfig hci0 up
> hciconfig -a

hci0:   Type: Primary  Bus: USB
        BD Address: 00:15:83:E8:3C:56  ACL MTU: 310:10  SCO MTU: 64:8
        UP RUNNING
        RX bytes:1180 acl:0 sco:0 events:64 errors:0
        TX bytes:1061 acl:0 sco:0 commands:64 errors:0
        Features: 0xff 0xff 0x8f 0xfe 0xdb 0xff 0x5b 0x87
        Packet type: DM1 DM3 DM5 DH1 DH3 DH5 HV1 HV2 HV3
        Link policy: RSWITCH HOLD SNIFF PARK
        Link mode: PERIPHERAL ACCEPT
        Name: 'stm32mp1'
        Class: 0x000000
        Service Classes: Unspecified
        Device Class: Miscellaneous,
        HCI Version: 4.0 (0x6)  Revision: 0x22bb
        LMP Version: 4.0 (0x6)  Subversion: 0x22bb
        Manufacturer: Cambridge Silicon Radio (10)
```

Skanowanie urządzeń:

```
> hcitool scan

Scanning ...
        70:1F:3C:xx:xx:xx       Galaxy M21
```

Włączenie widoczności SBC:

```
> hciconfig hci0 piscan
```

Wyłączenie widoczności SBC:

```
> hciconfig hci0 noscan
```

Skanowanie urządzeń *Low Energy*:

```
> bluetoothctl list

Controller 00:15:83:E8:3C:56 stm32mp1 [default]

> bluetoothctl scan le

Discovery started
[CHG] Controller 00:15:83:E8:3C:56 Discovering: yes
[NEW] Device 6C:D3:8E:76:89:71 TY
[NEW] Device 5F:CF:C0:FD:EE:F4 5F-CF-C0-FD-EE-F4
[NEW] Device 69:58:04:5F:EA:CC 69-58-04-5F-EA-CC
[CHG] Device 6C:D3:8E:76:89:71 ManufacturerData Key: 0x07d0
[CHG] Device 6C:D3:8E:76:89:71 ManufacturerData Value:
  80 03 00 00 0c 00 50 42 6e f7 9a b6 b2 b1 42 91  ......PBn.....B.
  9a 7a 13 6b 4c 32                                .z.kL2
[NEW] Device 5D:B0:A5:42:CE:39 5D-B0-A5-42-CE-39
[CHG] Device 5F:CF:C0:FD:EE:F4 RSSI: -82
[NEW] Device 55:40:E6:BF:2E:0A 55-40-E6-BF-2E-0A
[CHG] Device 5F:CF:C0:FD:EE:F4 RSSI: -72
```

## Omnikey 6321 CLi (iCLASS NFC card reader)

Strona produktu: https://www.hidglobal.com/products/6321-cli

Instalacja narzędzi PCSC:

```
> apt install pcsc-lite
> apt install pcsc-tools
> apt install opensc
```

Ze strony https://www3.hidglobal.com/drivers pobrać sterownik `ifdokccid_linux_v.4.3.2-1-d2622a7fbea0.tar.gz`.

Wypakować zagnieżdżone archiwum `ifdokccid_linux_arm-v.4.3.2-1-d2622a7fbea0.tar.gz`:

```
> tar -xzvf ifdokccid_linux_v.4.3.2-1-d2622a7fbea0.tar.gz
> cd ifdokccid_linux_v.4.3.2-1-d2622a7fbea0
> tar -xzvf ifdokccid_linux_arm-v.4.3.2-1-d2622a7fbea0.tar.gz
> cd ifdokccid_linux_arm-v.4.3.2-1-d2622a7fbea0
```

Zainstalować sterownik:

```
> ./install

Installing HID Global OMNIKEY CCID driver Smartcard reader driver ...

PCSC-Lite found: /usr/sbin/pcscd
Copying ifdokccid_linux_arm-v.4.3.2-1-d2622a7fbea0.bundle to /usr/lib/pcsc/drivers ...

Installation finished!
```

Test czytnika i kart iCLASS:

```
> pcsc_scan

PC/SC device scanner
V 1.6.2 (c) 2001-2022, Ludovic Rousseau <ludovic.rousseau@free.fr>
Using reader plug'n play mechanism
Scanning present readers...
0: OMNIKEY CardMan (076B:632A) 6321 CLi (USB iClass Reader) 00 00

Mon Oct  2 08:40:43 2023
 Reader 0: OMNIKEY CardMan (076B:632A) 6321 CLi (USB iClass Reader) 00 00
  Event number: 0
  Card state: Card removed,

Mon Oct  2 08:40:54 2023
 Reader 0: OMNIKEY CardMan (076B:632A) 6321 CLi (USB iClass Reader) 00 00
  Event number: 1
  Card state: Card inserted,
  ATR: 3B 8F 80 01 80 4F 0C A0 00 00 03 06 0A 00 18 00 00 00 00 7A

ATR: 3B 8F 80 01 80 4F 0C A0 00 00 03 06 0A 00 18 00 00 00 00 7A
+ TS = 3B --> Direct Convention
+ T0 = 8F, Y(1): 1000, K: 15 (historical bytes)
  TD(1) = 80 --> Y(i+1) = 1000, Protocol T = 0
-----
  TD(2) = 01 --> Y(i+1) = 0000, Protocol T = 1
-----
+ Historical bytes: 80 4F 0C A0 00 00 03 06 0A 00 18 00 00 00 00
  Category indicator byte: 80 (compact TLV data object)
    Tag: 4, len: F (initial access data)
      Initial access data: 0C A0 00 00 03 06 0A 00 18 00 00 00 00
+ TCK = 7A (correct checksum)

Possibly identified card (using /usr/share/pcsc/smartcard_list.txt):
3B 8F 80 01 80 4F 0C A0 00 00 03 06 0A 00 18 00 00 00 00 7A
3B 8F 80 01 80 4F 0C A0 00 00 03 06 .. 00 18 00 00 00 00 ..
        PicoPass 2KS (as per PCSC std part3)
3B 8F 80 01 80 4F 0C A0 00 00 03 06 0A 00 18 00 00 00 00 7A
3B 8F 80 01 80 4F 0C A0 00 00 03 06 0A .. .. 00 00 00 00 ..
        RFID - ISO 15693 Part 2 (as per PCSC std part3)
```

## Impulsator obrotowy inkrementalny

Opis: https://en.wikipedia.org/wiki/Incremental_encoder

Wymagania:
- Środowisko do budowy kernel Linux na SBC według [procedury](https://gitlab.com/wojrus-projects/embedded/potato_pi_m02_sw/-/tree/main/basic_boot_chain).

Bibliografia:
- https://www.kernel.org/doc/Documentation/input/devices/rotary-encoder.rst
- https://github.com/torvalds/linux/blob/master/Documentation/devicetree/bindings/input/rotary-encoder.txt

### Device Tree overlay

Impulsator wymaga `DT overlay`, którego kod znajduje się w pliku `$HOME/stm32mp1/overlays/rotary-encoder.dts` i jest automatycznie kompilowany po wykonaniu `./build_linux.sh`.
<br>Overlay musi być włączony w `u-boot` zgodnie z [procedurą](#device-tree-overlays).

### Podłączenie impulsatora do SBC

Podłączyć impulsator stykowy (pasywny) do złącza P1 **EXPANSION**:

| Name  | MPU pin | P1 pin |
|-------|---------|--------|
| OUT-A | PA0     | 40     |
| OUT-B | PA5     | 38     |
| GND   | -       | 39     |

Styki impulsatora będą zwierały wejścia MPU do masy. Wejścia mają włączone rezystory pullup.

### Test konfiguracji

Log `dmesg`:

```
> dmesg | grep rotary

[   19.129294] rotary-encoder rotary-encoder@0: binary
[   19.208163] input: rotary-encoder@0 as /devices/platform/rotary-encoder@0/input/input0
```

Konfiguracja pinów GPIO:

```
> gpioinfo -c gpiochip0

gpiochip0 - 16 lines:
        line   0:       "PA0"                   input active-low bias=pull-up consumer="rotary-encoder@0"
        line   1:       "PA1"                   input
        line   2:       "PA2"                   input
        line   3:       "PA3"                   input
        line   4:       "PA4"                   input
        line   5:       "PA5"                   input active-low bias=pull-up consumer="rotary-encoder@0"
        line   6:       "PA6"                   input
        line   7:       "PA7"                   input consumer="kernel"
        line   8:       "PA8"                   input
        line   9:       "PA9"                   input
        line  10:       "PA10"                  input
        line  11:       "PA11"                  input
        line  12:       "PA12"                  input
        line  13:       "PA13"                  output active-low consumer="red:heartbeat"
        line  14:       "PA14"                  input
        line  15:       "PA15"                  input
```

### Testy niskopoziomowe `/dev/input`

```
> ls -l /dev/input/by-path

lrwxrwxrwx 1 root root 9 Mar  3  2023 platform-rotary-encoder@0-event -> ../event0

> cat /dev/input/event0 | od -t x1 -w16

0000000 2f e6 1f 65 92 65 0d 00 03 00 00 00 01 00 00 00
0000020 2f e6 1f 65 92 65 0d 00 00 00 00 00 00 00 00 00
0000040 30 e6 1f 65 d5 58 00 00 03 00 00 00 02 00 00 00
0000060 30 e6 1f 65 d5 58 00 00 00 00 00 00 00 00 00 00
0000100 30 e6 1f 65 2c 27 0c 00 03 00 00 00 03 00 00 00
[...]
```

### Testy wysokopoziomowe `/dev/input`

Zainstalować `evemu-tools`:

```
> apt update
> apt install evemu-tools
```

Monitorowanie zdarzeń z impulsatora:

```
> evemu-record /dev/input/event0

# EVEMU 1.3
# Kernel: 6.1.28
# Input device name: "rotary-encoder@0"
# Input device ID: bus 0x19 vendor 0000 product 0000 version 0000
[...]
################################
#      Waiting for events      #
################################
E: 0.000001 0003 0000 0001      # EV_ABS / ABS_X                1
E: 0.000001 0000 0000 0000      # ------------ SYN_REPORT (0) ---------- +0ms
E: 0.232950 0003 0000 0002      # EV_ABS / ABS_X                2
E: 0.232950 0000 0000 0000      # ------------ SYN_REPORT (0) ---------- +232ms
E: 0.591906 0003 0000 0003      # EV_ABS / ABS_X                3
E: 0.591906 0000 0000 0000      # ------------ SYN_REPORT (0) ---------- +359ms
E: 1.440519 0003 0000 0002      # EV_ABS / ABS_X                2
E: 1.440519 0000 0000 0000      # ------------ SYN_REPORT (0) ---------- +849ms
E: 1.574490 0003 0000 0001      # EV_ABS / ABS_X                1
E: 1.574490 0000 0000 0000      # ------------ SYN_REPORT (0) ---------- +134ms
E: 1.729685 0003 0000 0000      # EV_ABS / ABS_X                0
E: 1.729685 0000 0000 0000      # ------------ SYN_REPORT (0) ---------- +155ms
[...]
```

```
> evtest

No device specified, trying to scan all of /dev/input/event*
Available devices:
/dev/input/event0:      rotary-encoder@0
Select the device event number [0-0]: 0
Input driver version is 1.0.1
Input device ID: bus 0x19 vendor 0x0 product 0x0 version 0x0
Input device name: "rotary-encoder@0"
Supported events:
  Event type 0 (EV_SYN)
  Event type 3 (EV_ABS)
    Event code 0 (ABS_X)
      Value      0
      Min        0
      Max       24
      Flat       1
Properties:
Testing ... (interrupt to exit)
Event: time 1696589297.223036, type 3 (EV_ABS), code 0 (ABS_X), value 1
Event: time 1696589297.223036, -------------- SYN_REPORT ------------
Event: time 1696589297.358961, type 3 (EV_ABS), code 0 (ABS_X), value 2
Event: time 1696589297.358961, -------------- SYN_REPORT ------------
Event: time 1696589297.720151, type 3 (EV_ABS), code 0 (ABS_X), value 3
Event: time 1696589297.720151, -------------- SYN_REPORT ------------
Event: time 1696589298.401136, type 3 (EV_ABS), code 0 (ABS_X), value 4
Event: time 1696589298.401136, -------------- SYN_REPORT ------------
Event: time 1696589298.922510, type 3 (EV_ABS), code 0 (ABS_X), value 3
Event: time 1696589298.922510, -------------- SYN_REPORT ------------
Event: time 1696589299.116657, type 3 (EV_ABS), code 0 (ABS_X), value 2
Event: time 1696589299.116657, -------------- SYN_REPORT ------------
Event: time 1696589299.287455, type 3 (EV_ABS), code 0 (ABS_X), value 1
Event: time 1696589299.287455, -------------- SYN_REPORT ------------
Event: time 1696589299.403173, type 3 (EV_ABS), code 0 (ABS_X), value 0
Event: time 1696589299.403173, -------------- SYN_REPORT ------------
[...]
```

## Czujnik temperatury TMP75C

Strona produktu: https://www.ti.com/product/TMP75C

Wymagania:
- Środowisko do budowy kernel Linux na SBC według [procedury](https://gitlab.com/wojrus-projects/embedded/potato_pi_m02_sw/-/tree/main/basic_boot_chain).

### Zbudowanie sterownika kernel Linux

```
#
# Configuration stage
#

> cd $HOME/stm32mp1
> source prepare_env.sh
> cd stm32mp1-openstlinux-6.1-yocto-mickledore-mp1-v23.06.21/sources/arm-ostl-linux-gnueabi/linux-stm32mp-6.1.28-stm32mp-r1-r0/linux-6.1.28
> make ARCH=arm menuconfig

Device Drivers > Hardware Monitoring support

<M> National Semiconductor LM75 and compatibles

#
# Build stage
#

> cd $HOME/stm32mp1
> ./build_linux.sh
> ./build_ext4.sh
> ./sd_card_write /dev/SD_CARD_DEVICE
```

### Device Tree overlay

Sensor wymaga `DT overlay`, którego kod znajduje się w pliku `$HOME/stm32mp1/overlays/tmp75c-i2c5.dts` i jest automatycznie kompilowany po wykonaniu `./build_linux.sh`.
<br>Overlay musi być włączony w `u-boot` zgodnie z [procedurą](#device-tree-overlays).

### Podłączenie sensora do SBC

Podłączyć sensor do I2C #5 w złączu P1 **EXPANSION**:

| Name   | MPU pin | P1 pin |
|--------|---------|--------|
| SCL    | PH13    | 5      |
| SDA    | PE13    | 3      |
| +3.3 V | -       | 1      |
| GND    | -       | 6      |

### Testy sensora

```
> dmesg | grep lm75

[   15.927050] lm75 1-0048: probe
[   15.927112] lm75 1-0048: Looking up vs-supply from device tree
[   15.927131] lm75 1-0048: Looking up vs-supply property in node /soc/etzpc@5c007000/i2c@4c006000/tmp75c@48 failed
[   15.927187] lm75 1-0048: supply vs not found, using dummy regulator
[   15.972080] lm75 1-0048: hwmon1: sensor 'tmp75c'
[   16.041560] i2c-core: driver [lm75] registered
```

```
> ls -la /sys/class/hwmon

lrwxrwxrwx  1 root root 0 Mar  3  2023 hwmon0 -> ../../devices/virtual/thermal/thermal_zone0/hwmon0
lrwxrwxrwx  1 root root 0 Mar  3  2023 hwmon1 -> ../../devices/platform/soc/5c007000.etzpc/4c006000.i2c/i2c-1/1-0048/hwmon/hwmon1

> cd /sys/class/hwmon/hwmon1
> cat name

tmp75c
```

Odczyt temperatury (wynik podzielić przez 1000):

```
> cat temp1_input

26062
```

Odczyt rejestrów T_high/T_low (wynik podzielić przez 1000):

```
> cat temp1_max temp1_max_hyst

80000
75000
```

## ADC Microchip MCP3426

Strona produktu: https://www.microchip.com/en-us/product/MCP3426

Wymagania:
- Środowisko do budowy kernel Linux na SBC według [procedury](https://gitlab.com/wojrus-projects/embedded/potato_pi_m02_sw/-/tree/main/basic_boot_chain).

### Zbudowanie sterownika kernel Linux

```
#
# Configuration stage
#

> cd $HOME/stm32mp1
> source prepare_env.sh
> cd stm32mp1-openstlinux-6.1-yocto-mickledore-mp1-v23.06.21/sources/arm-ostl-linux-gnueabi/linux-stm32mp-6.1.28-stm32mp-r1-r0/linux-6.1.28
> make ARCH=arm menuconfig

Device Drivers > Industrial I/O support (IIO) > Analog to digital converters

<M> Microchip Technology MCP3421/2/3/4/5/6/7/8 driver

#
# Build stage
#

> cd $HOME/stm32mp1
> ./build_linux.sh
> ./build_ext4.sh
> ./sd_card_write /dev/SD_CARD_DEVICE
```

### Device Tree overlay

Sensor wymaga `DT overlay`, którego kod znajduje się w pliku `$HOME/stm32mp1/overlays/mcp3426-i2c5.dts` i jest automatycznie kompilowany po wykonaniu `./build_linux.sh`.
<br>Overlay musi być włączony w `u-boot` zgodnie z [procedurą](#device-tree-overlays).

### Podłączenie ADC do SBC

Podłączyć ADC do I2C #5 w złączu P1 **EXPANSION**:

| Name   | MPU pin | P1 pin |
|--------|---------|--------|
| SCL    | PH13    | 5      |
| SDA    | PE13    | 3      |
| +3.3 V | -       | 1      |
| GND    | -       | 6      |

### Diagnostyka ADC

```
> modprobe mcp3422

> ls -la /sys/bus/iio/devices

lrwxrwxrwx 1 root root 0 Oct  7 14:23 iio:device0 -> ../../../devices/platform/soc/5c007000.etzpc/4c006000.i2c/i2c-1/1-0068/iio:device0

> iio_info

Library version: 0.23 (git tag: 92d6a35)
Compiled with backends: local xml ip usb serial
IIO context created with local backend.
Backend version: 0.23 (git tag: 92d6a35)
Backend description string: Linux stm32mp1 6.1.28 #20 SMP PREEMPT Sat Oct  7 14:25:07 CEST 2023 armv7l
IIO context has 2 attributes:
        local,kernel: 6.1.28
        uri: local:
IIO context has 1 devices:
        iio:device0: 1-0068 (label: ADC 16-bit Delta-Sigma)
                2 channels found:
                        voltage0:  (input)
                        4 channel-specific attributes found:
                                attr  0: raw value: 0
                                attr  1: sampling_frequency value: 240
                                attr  2: scale value: 0.001000000
                                attr  3: scale_available value: 0.001000000 0.000500000 0.000250000 0.000125000
                        voltage1:  (input)
                        4 channel-specific attributes found:
                                attr  0: raw value: 0
                                attr  1: sampling_frequency value: 240
                                attr  2: scale value: 0.001000000
                                attr  3: scale_available value: 0.001000000 0.000500000 0.000250000 0.000125000
                2 device-specific attributes found:
                                attr  0: sampling_frequency_available value: 240 60 15
                                attr  1: waiting_for_supplier value: 0
                No trigger on this device

> cd /sys/bus/iio/devices/iio:device0/
> ls -la

-rw-r--r-- 1 root root 4096 Oct  7 14:23 in_voltage0_raw
-rw-r--r-- 1 root root 4096 Oct  7 14:23 in_voltage0_scale
-rw-r--r-- 1 root root 4096 Oct  7 14:23 in_voltage1_raw
-rw-r--r-- 1 root root 4096 Oct  7 14:23 in_voltage1_scale
-rw-r--r-- 1 root root 4096 Oct  7 14:23 in_voltage_sampling_frequency
-r--r--r-- 1 root root 4096 Oct  7 14:23 in_voltage_scale_available
-r--r--r-- 1 root root 4096 Oct  7 14:23 label
-r--r--r-- 1 root root 4096 Oct  7 14:23 name
lrwxrwxrwx 1 root root    0 Oct  7 14:23 of_node -> ../../../../../../../../firmware/devicetree/base/soc/etzpc@5c007000/i2c@4c006000/mcp3426@68
drwxr-xr-x 2 root root    0 Oct  7 14:23 power
-r--r--r-- 1 root root 4096 Oct  7 14:23 sampling_frequency_available
lrwxrwxrwx 1 root root    0 Oct  7 14:23 subsystem -> ../../../../../../../../bus/iio
-rw-r--r-- 1 root root 4096 Oct  7 14:23 uevent
-r--r--r-- 1 root root 4096 Oct  7 14:23 waiting_for_supplier

> cat label

ADC 16-bit Delta-Sigma
```

### Odczyt ADC

```
> cat in_voltage0_raw in_voltage1_raw
```

## Protokół One-Wire (OWFS)

Wymagania (przykład do testu):
- Interfejs USB->RS232 np. https://ftdichip.com/products/chipi-x10/
- Moduł z układem DS2480B
- Czujniki temperatury DS18B20 itd.

### Instalacja usługi OWFS

```
# Install OWFS:
> apt install owfs

# Configure service (change line `ExecStart`):
> nano /lib/systemd/system/owfs.service

ExecStart=/usr/bin/owfs --device=/dev/ttyUSB0 --allow_other %t/owfs

# Restart OS:
> reboot

# Check service status:
> systemctl status owfs.service

* owfs.service - 1-wire filesystem FUSE mount
     Loaded: loaded (/lib/systemd/system/owfs.service; enabled; preset: enabled)
     Active: active (running) since Mon 2023-10-09 09:41:34 UTC; 6min ago
       Docs: man:owfs(1)
   Main PID: 529 (owfs)
     Memory: 1.2M
     CGroup: /system.slice/owfs.service
             `-529 /usr/bin/owfs --device=/dev/ttyUSB0 --allow_other /run/owfs

Oct 09 09:41:32 stm32mp1 systemd[1]: Starting 1-wire filesystem FUSE mount...
Oct 09 09:41:34 stm32mp1 systemd[1]: Started 1-wire filesystem FUSE mount.
```

### Test OWFS

Lista 4 czujników temperatury:

```
> ls -la /run/owfs/uncached

drwxrwxrwx 1 root root 4096 Oct  9 10:16 10.2C80F1010800
drwxrwxrwx 1 root root 4096 Oct  9 10:16 10.3470F1010800
drwxrwxrwx 1 root root 4096 Oct  9 10:16 10.488BF1010800
drwxrwxrwx 1 root root 4096 Oct  9 10:16 10.4D4ADD010800
[...]
```

Odczyt temperatury:

```
> cat /run/owfs/uncached/10.2C80F1010800/temperature
```

## Wyjście VGA

SBC posiada analogowe wyjście video w standardzie VGA (gniazdo DSUB-15 female).<br>
Obecnie obsługiwane są rozdzielczości:
- 640 x 480 @ 60 Hz (standardowa konfiguracja)
- 800 x 600 @ 60 Hz (wymaga aktywacji DT overlay)
- 1024 x 768 @ 60 Hz (--//--)
- 1366 x 768 @ 60 Hz (--//--)

Głębia kolorów:
- 16 bitów (RGB565) w buforze ramki w Linux
- 12 bitów (RGB444) w złączu VGA

Konwersja kolorów 16->12 bitów jest sprzętowa, przez odcięcie nadmiarowych bitów LSB.

Standardowo SBC uruchamia się w trybie 640 x 480.<br>
Pozostałe rozdzielczości można włączyć przez aktywacje pliku DT overlay w `u-boot` zgodnie z procedurą [Device Tree overlays](#device-tree-overlays).

Przykład:

```
> env set fdtoverlay_files vga_800x600
> env save
> reset
```

Dostępne pliki overlays:
- vga_800x600
- vga_1024x768
- vga_1366x768

## LCD SPI ILI9341

Do testów był używany moduł wyświetlacza 320x240 2.4" z kontrolerem ILI9341:
- https://nettigo.pl/products/wyswietlacz-lcd-tft-2-4-ze-sterownikiem-ili9341-dotykowy
- https://www.gotronik.pl/wyswietlacz-lcd-tft-przekatna-24-240x320-spi-ili9341-p-4013.html
- https://kamami.pl/wyswietlacze-tftips/579362-modul-wyswietlacza-tft-24-z-panelem-dotykowym-i-gniazdem-sd-modtftlcd24spi.html

Wymagania:
- Środowisko do budowy kernel Linux na SBC według [procedury](https://gitlab.com/wojrus-projects/embedded/potato_pi_m02_sw/-/tree/main/basic_boot_chain).

### Zbudowanie sterownika kernel Linux

```
#
# Configuration stage
#

> cd $HOME/stm32mp1
> source prepare_env.sh
> cd stm32mp1-openstlinux-6.1-yocto-mickledore-mp1-v23.06.21/sources/arm-ostl-linux-gnueabi/linux-stm32mp-6.1.28-stm32mp-r1-r0/linux-6.1.28
> make ARCH=arm menuconfig

- Device drivers > Graphics support

    <M> DRM support for ILI9341 display panels 
    Backlight & LCD device support --->
        {*} Lowlevel Backlight controls
            <*> Generic GPIO based Backlight Driver
    Console display driver support --->
        [*] Framebuffer Console support 

#
# Build stage
#

> cd $HOME/stm32mp1
> ./build_linux.sh
> ./build_ext4.sh
> ./sd_card_write /dev/SD_CARD_DEVICE
```

### Device Tree overlay

Wyświetlacz wymaga `DT overlay`, którego kod znajduje się w pliku `$HOME/stm32mp1/overlays/lcd-ili9341-spi1.dts` i jest automatycznie kompilowany po wykonaniu `./build_linux.sh`.
<br>Overlay musi być włączony w `u-boot` zgodnie z [procedurą](#device-tree-overlays).

Przykład:

```
> env set fdtoverlay_files lcd-ili9341-spi1
> env save
> reset
```

### Podłączenie LCD do SBC

Podłączyć moduł LCD do SPI #1 w złączu P1 **EXPANSION**:

| Name      | MPU pin | P1 pin      |
|-----------|---------|-------------|
| +3.3 V    | -       | 1           |
| GND       | -       | 6           |
| LCD_RESET | PA0     | 40          |
| LCD_CD    | PA5     | 38          |
| LCD_LED   | -       | 17 (+3.3 V) |
| SPI_CS    | PF12    | 24          |
| SPI_CLK   | PB1     | 23          |
| SPI_MOSI  | PC0     | 19          |
| SPI_MISO  | PC3     | 21          |

### Testy LCD

Po włączeniu overlay i resecie SBC na LCD powinno uruchomić się środowisko graficzne `Weston` (przełączy się z wyjścia VGA).

```
> dmesg | grep ili9341

[   24.117925] [drm] Initialized ili9341 1.0.0 20180514 for spi1.0 on minor 1
[   24.230563] ili9341 spi1.0: [drm] fb1: ili9341drmfb frame buffer device
```

```
> modetest -M ili9341

[...]

Connectors:
id      encoder status          name            size (mm)       modes   encoders
31      35      connected       SPI-1           49x37           1       35

[...]
```

```
> drmdevice

--- Checking the number of DRM device available ---
--- Devices reported 2 ---
--- Retrieving devices information (PCI device revision is ignored) ---
device[0]
+-> available_nodes 0x01
+-> nodes
|   +-> nodes[0] /dev/dri/card1
+-> bustype 0002
|   +-> platform
|       +-> fullname    /soc/spi@44004000/display@0
+-> deviceinfo
    +-> platform
        +-> compatible
                    adafruit,yx240qv29
                    ilitek,ili9341

[...]

device[1]
+-> available_nodes 0x01
+-> nodes
|   +-> nodes[0] /dev/dri/card0
+-> bustype 0002
|   +-> platform
|       +-> fullname    /soc/display-controller@5a001000
+-> deviceinfo
    +-> platform
        +-> compatible
                    st,stm32-ltdc
[...]
```

## Stereo Audio DAC USB PCM2707

Wymagania:
- Środowisko do budowy kernel Linux na SBC według [procedury](https://gitlab.com/wojrus-projects/embedded/potato_pi_m02_sw/-/tree/main/basic_boot_chain).

Kernel posiada już odpowiednie sterowniki więc wystarczy podłączyć DAC do USB.

Detekcja karty dźwiękowej:

```
> usb-devices

[...]

T:  Bus=01 Lev=01 Prnt=01 Port=01 Cnt=01 Dev#=  5 Spd=12   MxCh= 0
D:  Ver= 1.10 Cls=00(>ifc ) Sub=00 Prot=00 MxPS= 8 #Cfgs=  1
P:  Vendor=08bb ProdID=2707 Rev=01.00
S:  Manufacturer=Burr-Brown from TI
S:  Product=USB Audio DAC
C:  #Ifs= 3 Cfg#= 1 Atr=80 MxPwr=500mA
I:  If#= 0 Alt= 0 #EPs= 0 Cls=01(audio) Sub=01 Prot=00 Driver=snd-usb-audio
I:  If#= 1 Alt= 0 #EPs= 0 Cls=01(audio) Sub=02 Prot=00 Driver=snd-usb-audio
I:  If#= 2 Alt= 0 #EPs= 1 Cls=03(HID  ) Sub=00 Prot=00 Driver=usbhid
E:  Ad=85(I) Atr=03(Int.) MxPS=   1 Ivl=10ms

[...]

> cat /proc/asound/cards

0 [DAC            ]: USB-Audio - USB Audio DAC
                      Burr-Brown from TI USB Audio DAC at usb-5800c000.usb-2, full speed
                      
> aplay -l

**** List of PLAYBACK Hardware Devices ****
card 0: DAC [USB Audio DAC], device 0: USB Audio [USB Audio]
  Subdevices: 1/1
  Subdevice #0: subdevice #0
```

Test ALSA:

```
# Set volume to safe level (~10%):
> su -l "weston" -c "alsamixer"

> su -l "weston" -c "amixer"

> speaker-test -D hw:0 -c 2 -F S16_LE -f 440 -t sine -l 1

> aplay -D hw:0 /usr/share/sounds/alsa/Front_Left.wav
```

Test PulseAudio:

```
> su -l "weston" -c "pactl info"

Server String: unix:/run/user/1000/pulse/native
Library Protocol Version: 35
Server Protocol Version: 35
Is Local: yes
Client Index: 0
Tile Size: 65496
User Name: weston
Host Name: stm32mp1
Server Name: pulseaudio
Server Version: 16.1
Default Sample Specification: s16le 2ch 44100Hz
Default Channel Map: front-left,front-right
Default Sink: alsa_output.usb-Burr-Brown_from_TI_USB_Audio_DAC-00.analog-stereo
Default Source: alsa_output.usb-Burr-Brown_from_TI_USB_Audio_DAC-00.analog-stereo.monitor
Cookie: 4c8a:bff7

> su -l "weston" -c "pactl list cards"

Card #0
        Name: alsa_card.usb-Burr-Brown_from_TI_USB_Audio_DAC-00
        Driver: module-alsa-card.c
        Owner Module: 6
        Properties:
                alsa.card = "0"
                alsa.card_name = "USB Audio DAC"
                alsa.long_card_name = "Burr-Brown from TI USB Audio DAC at usb-5800c000.usb-2, full speed"
                alsa.driver_name = "snd_usb_audio"
                device.bus_path = "platform-5800c000.usb-usb-0:2:1.0"
                sysfs.path = "/devices/platform/soc/5800c000.usb/usb2/2-2/2-2:1.0/sound/card0"
                udev.id = "usb-Burr-Brown_from_TI_USB_Audio_DAC-00"
                device.bus = "usb"
                device.vendor.id = "08bb"
                device.vendor.name = "Texas Instruments"
                device.product.id = "2707"
                device.product.name = "PCM2707 stereo audio DAC"
                device.serial = "Burr-Brown_from_TI_USB_Audio_DAC"
                device.string = "0"
                device.description = "PCM2707 stereo audio DAC"
                module-udev-detect.discovered = "1"
                device.icon_name = "audio-card-usb"
        Profiles:
                output:analog-stereo: Analog Stereo Output (sinks: 1, sources: 0, priority: 6500, available: yes)
                output:iec958-stereo: Digital Stereo (IEC958) Output (sinks: 1, sources: 0, priority: 5500, available: yes)
                off: Off (sinks: 0, sources: 0, priority: 0, available: yes)
        Active Profile: output:analog-stereo
        Ports:
                analog-output: Analog Output (type: Analog, priority: 9900, latency offset: 0 usec, availability unknown)
                        Part of profile(s): output:analog-stereo
                iec958-stereo-output: Digital Output (S/PDIF) (type: SPDIF, priority: 0, latency offset: 0 usec, availability unknown)
                        Part of profile(s): output:iec958-stereo
```

Odtworzenie MP3:

```
> su -l "weston" -c "mpg123 FILE_NAME.mp3"
```

# Licencja

MIT
